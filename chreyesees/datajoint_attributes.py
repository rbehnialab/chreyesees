import datajoint as dj
import pickle


class Pickle(dj.AttributeAdapter):
    
    @property
    def attribute_type(self):
        return 'longblob'
    
    def get(self, value):
        return pickle.loads(value)
    
    def put(self, obj):
        return pickle.dumps(obj)
    
pickletype = Pickle() 