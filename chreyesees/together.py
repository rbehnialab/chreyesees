# utility functions for compiling responses
import pandas as pd
import numpy as np
from chreyesees import colormedulla
from chreyesees import OPSINS, LEDS
import puffbird as pb
from chreyesees import transform
from chreyesees.analysis import CI_INTERVAL


def get_Q_given_other(
    k, k_other, cell_types,
):
    r_infos = []
    for c in cell_types:
        k['cell_type'] = c
        k_other['cell_type'] = c
        
        nans = ((
            colormedulla.AllRegression() * colormedulla.AllRegression.GroupedMeans) & k_other).fetch1(
                'allnan')

        X = (colormedulla.AllDataset() & k).fetch1('inputs')
        Q = (colormedulla.AllCaptures & k).fetch1('captures')
        Q = Q[~nans]
        X = X[~nans]
        
        r_info = pd.DataFrame(np.hstack([
            Q, X,
        ]), columns=OPSINS+LEDS)
        r_info['cell_type'] = c
        r_infos.append(r_info)
        
    r_infos = pd.concat(r_infos)
        
    return r_infos

def get_compiled_dataframes(
    k, cell_types, with_psth=False, psth_select=False, return_all_data=False, 
    postfix=''
):
    ## put together all the responses
    # to create dataframe of summary statistic of each cell_type
    cell_infos = []
    # to create dataframe of summary response statistics of group
    r_infos = []
    all_data = []

    for c in cell_types:
        k['cell_type'] = c
        if return_all_data:
            if not len(colormedulla.AllDataset & k):
                continue
            Y, X, snr, var = (colormedulla.AllDataset() & k).fetch1('outputs', 'inputs', 'snr', 'var')
        
            all_data.append({
                'cell_type': c, 
                'outputs': Y, 
                'inputs': X, 
                'snr': snr, 
                'var': var
            })
            continue
        
        if not len(colormedulla.AllRegression.GroupedMeans & k):
            continue
        
        if with_psth:
            counts, bs_counts, r, bs_means, bs_vars, bs_low, bs_high, bs_sd, nans, mean_psth, psth_low, psth_high, psth_sd = ((colormedulla.AllRegression() * colormedulla.AllRegression.GroupedMeans) & k).fetch1(
                'counts', 'bs_counts',
                f'mean_outputs{postfix}', f'bs_means{postfix}', 'bs_est_var', 
                f'bs_low{postfix}', f'bs_high{postfix}', f'bs_sd{postfix}', 
                'allnan', f'mean_psth{postfix}', f'psth_low{postfix}', f'psth_high{postfix}', f'psth_sd{postfix}')
        else:
            counts, bs_counts, r, bs_means, bs_vars, bs_low, bs_high, bs_sd, nans = ((colormedulla.AllRegression() * colormedulla.AllRegression.GroupedMeans) & k).fetch1(
                'counts', 'bs_counts',
                f'mean_outputs{postfix}', f'bs_means{postfix}', 'bs_est_var', f'bs_low{postfix}', 
                f'bs_high{postfix}', f'bs_sd{postfix}', 'allnan')
            
        if psth_select:
            psth_rois =  ((colormedulla.AllRegression() * colormedulla.AllRegression.GroupedMeans) & k).fetch1('psth_select')
        
        head_info, Y, X, sig, snr, auc = (colormedulla.AllDataset() & k).fetch1('info', 'outputs', 'inputs', 'sig', 'snr', 'auc')

        cell_info = pd.DataFrame(head_info, columns=['stimulus_set', 'recording_id', 'roi_id'])
        cell_info['sig'] = sig
        cell_info['snr'] = snr
        cell_info['auc'] = auc
        
        labels = (colormedulla.AllRegression & k).fetch1('labels')
        cell_info['labels'] = labels
        
        is_response = np.isfinite(Y)
        
        lbool = labels == k['group_id']
        cell_info['is_group'] = lbool
        cell_info['cell_type'] = c
        
        cell_infos.append(cell_info)
        
        Q = (colormedulla.AllCaptures & k).fetch1('captures')
        Q = Q[~nans]
        X = X[~nans]
        
        r_info = pd.DataFrame(np.hstack([
            Q, X, r[:, None], bs_low[:, None], 
            bs_high[:, None], bs_sd[:, None], 
            counts[:, None]
        ]), columns=OPSINS+LEDS+['r', 'low', 'high', 'sd', 'counts'])
        r_info['cell_type'] = c
        gamut_bool = is_response[:, lbool & (head_info[:, 0] == 'gamut')][~nans].any(-1)
        r_info['gamut_bool'] = gamut_bool
        expl_bool = is_response[:, lbool & (head_info[:, 0] == 'exploration')][~nans].any(-1)
        r_info['expl_bool'] = expl_bool
        # nstim x nbootstraps
        r_info[f'bs_means'] = list(bs_means.T)
        r_info['bs_counts'] = list(bs_counts.T)
        
        if with_psth:
            r_info[f'psth'] = list(mean_psth)
            r_info[f'psth_low'] = list(psth_low)
            r_info[f'psth_high'] = list(psth_high)
            r_info[f'psth_sd'] = list(psth_sd)
        # single rois
        if psth_select:
            r_info['psth_rois'] = list(psth_rois)
        
        r_infos.append(r_info)
    
    if return_all_data:
        return pd.DataFrame(all_data)
    
    cell_infos = pd.concat(cell_infos)
    r_infos = pd.concat(r_infos)
    
    return cell_infos, r_infos


def get_sparsity(key, cell_types):
    cell_types = [
        {'cell_type': ct}
        for ct in cell_types
    ]
    table = colormedulla.SparsityIndex & key & cell_types
    df = table.fetch(format='frame').reset_index()
    return df


def get_rayleigh(key, cell_types):
    cell_types = [
        {'cell_type': ct}
        for ct in cell_types
    ]
    table = colormedulla.RayleighIndex & key & cell_types
    df = table.fetch(format='frame').reset_index()
    return pb.puffy_to_long(
        df, 
        bs_iter={
            'rayleigh_bs_hard': 0, 
            'rayleigh_bs': 0, 
            'rayleigh_bs_vec_hard': 0,
            'rayleigh_bs_vec': 0,
        }, 
        vec_dim={
            'rayleigh_bs_vec_hard': 1,
            'rayleigh_bs_vec': 1,
        },
    )


def process_r_infos_iso(r_infos, wiggle=0.05, transform_func=lambda x: x):
    # get isoluminant close responses for r_infos
    r_gamut = r_infos[r_infos['gamut_bool']]
    r_gamut
    E = transform_func(r_gamut[OPSINS])
    E['mean'] = E.mean(1)
    emean = E['mean'].mean()
    
    means = transform_func(r_infos[OPSINS].to_numpy()).mean(1)
    
    mbool = ((means > (emean - wiggle)) & (means < (emean + wiggle)))
    return r_infos[mbool]
    

def get_subspace_data(key):
    data = (colormedulla.AllRegression & key).fetch(format='frame')
    info = pb.puffy_to_long(data[['proportions', 'edge_centers', 'mean_angle']], pos={'proportions': 0, 'edge_centers': 0})
    info_df = (colormedulla.AllDataset & key).proj('info').fetch(format='frame')['info'].apply(
        lambda x: pd.Series(list(x.T), index=['stimulus_set', 'recording_id', 'cell_id'])
    )[['stimulus_set']]
    angles = pb.puffy_to_long(pd.merge(info_df.reset_index(), data[['angles', 'mean_angle']].reset_index(),), roi_idx={'stimulus_set': 0, 'angles': 0})
    angles = pd.merge(angles, data['rots'].reset_index())
    return data, info, info_df, angles

def get_encoding_stats(key_model, model_like=None, postfix='', cell_types=None):
    data = (colormedulla.AllModelsCV() * colormedulla.ModelSettings()) & key_model
    # get counts
    # bs_data = (colormedulla.AllModelsCV.Bootstrap) & key_model
    if cell_types is not None:
        restrict = [{'cell_type': ct} for ct in cell_types]
        data = data & restrict
        # bs_data = bs_data & restrict
    if model_like is not None:
        data = data & model_like
        # bs_data = bs_data & model_like
        
    # bs_data = bs_data.proj(
    #     f'bs_params{postfix}', f'bs_model{postfix}'
    # ).fetch(format='frame').rename(columns=lambda x: x.replace(postfix, ''))
    # bs_data = bs_data.rename(columns=lambda x: x.replace(postfix, ''))  
    
    data = data.proj(
        f'scores{postfix}', f'final_params{postfix}', f'r2s{postfix}', 
        f'r2ests{postfix}', f'r2{postfix}', 
        f'params{postfix}', f'outputs{postfix}', f'outputs_pred{postfix}', 
        'input_idcs', 'inputs', 'counts', f'final_model{postfix}', 'transform_func'
    ).fetch(format='frame')
    data = data.rename(columns=lambda x: x.replace(postfix, ''))
    print(data.columns)
    print(data.index.names)
    # data = data.join(counts, on=list(set(counts.index.names) & set(data.index.names)))
    # data = pd.concat([data, counts], axis=1)
    scores = pb.puffy_to_long(data['scores'])
    r2s = pb.puffy_to_long(data['r2s'])
    r2ests = pb.puffy_to_long(data['r2ests'])
    r2_whole = data['r2'].reset_index()
    params = data['final_params'].apply(pd.Series, dtype=object)
    data = pd.concat([data, params], axis=1)
    # bs_params = bs_data['bs_params'].apply(pd.Series, dtype=object)
    cv_params = data['params'].apply(pd.Series, dtype=object)
    cv_params = pb.puffy_to_long(
        cv_params, 
        cv_idx={col: 0 for col in cv_params.columns}, 
        max_depth=1
    ).set_index(list(cv_params.index.names)+['cv_idx'])
    bs_params = cv_params
    
    # function to rename model_names
    # rename = lambda x: (('-'.join(x.split('-')[:-2])))
    rename = lambda x: x
    
    try:
        have_tau = True
        tau = params['tau_'].apply(pd.Series)
        bs_tau = bs_params['tau_'].apply(pd.Series)
    except:
        have_tau = False
        tau = pd.DataFrame([[np.nan]]*len(params), index=params.index).dropna()
        bs_tau = pd.DataFrame([[np.nan]]*len(bs_params), index=bs_params.index).dropna()
        
    def process_column(tau, bs_tau, col, idx):
        tau = tau.iloc[:, [idx]]
        bs_tau = bs_tau.iloc[:, [idx]]
        tau.columns = [col]
        bs_tau.columns = [col]
        tau = tau.dropna()
        bs_tau = bs_tau.dropna()
        tau = tau.reset_index()
        bs_tau = bs_tau.reset_index()
        tau['names'] = tau['model_name'].apply(rename)
        bs_tau['names'] = bs_tau['model_name'].apply(rename)
        bs_low = bs_tau.groupby(list(set(tau.columns) - set([col])))[col].apply(np.percentile, q=CI_INTERVAL[0]).reset_index()
        bs_low = bs_low.rename(columns={col: f'{col}_low'})
        bs_high = bs_tau.groupby(list(set(tau.columns) - set([col])))[col].apply(np.percentile, q=CI_INTERVAL[1]).reset_index()
        bs_high = bs_high.rename(columns={col: f'{col}_high'})
        tau = pd.merge(tau, bs_low)
        tau = pd.merge(tau, bs_high)
        return {
            col: tau, 
            f'bs_{col}': bs_tau
        }
        
    # three max params currently
    col_dict = {}
    col_allowed = ['tau', 'alpha', 'beta']
    if have_tau:
        for col_idx, col_name in zip(range(len(tau.columns)), col_allowed):
            col_dict.update(process_column(tau, bs_tau, col_name, col_idx))
    
    for col in col_allowed:
        if col not in col_dict:
            col_dict[col] = None
            col_dict[f'bs_{col}'] = None
        
    
    scores['names'] = scores['model_name'].apply(rename)
    r2s['names'] = r2s['model_name'].apply(rename)
    r2ests['names'] = r2ests['model_name'].apply(rename)
    r2_whole['names'] = r2_whole['model_name'].apply(rename)
    
    y_vs_pred = pb.puffy_to_long(data[['outputs', 'outputs_pred']], stim_idx={'outputs': 0, 'outputs_pred': 0})
    y_vs_pred['names'] = y_vs_pred['model_name'].apply(rename)
    
    coefs = {}
    for name, params_ in zip(['coef', 'bs_coef'], [params, bs_params]):
        coef = data[['input_idcs', 'transform_func']].join(params_['coef_']).dropna()
        # coef = pd.concat([params_[['coef_']], data[['input_idcs']]], axis=1).dropna()
        dbool = coef['transform_func'].apply(lambda x: 'pca' in x)
        print(dbool.any())
        print(np.linalg.inv(transform.bigA))
        coef['coef_'] = coef.apply(
            lambda series: (
                series['coef_'] @ np.linalg.inv(transform.bigA)
                if 'pca' in series['transform_func'] and (len(series['coef_']) == 4)
                else np.concatenate([
                    series['coef_'][:1],
                    series['coef_'][1:] @ np.linalg.inv(transform.bigA), 
                ])
                if 'pca' in series['transform_func'] and (len(series['coef_']) == 5)
                else
                series['coef_']
            ),
            axis=1
        )
        print(coef['transform_func'].unique())
        coef = coef.drop(columns=['transform_func'])
        coef['coef_'] = coef['coef_'].apply(lambda x: x / np.linalg.norm(x, ord=1))
        coef['input_idcs'] = coef[['coef_', 'input_idcs']].apply(
            lambda x: (
                x['input_idcs'] 
                if (len(x['input_idcs']) == len(x['coef_']))
                else -np.arange(1, len(x['coef_'])+1).astype(int)
            ), 
            axis=1
        )
        # print(params_)
        # print(coef[['coef_', 'input_idcs']].apply(lambda x: len(x['coef_']) == len(x['input_idcs']), axis=1).all())
        coef = pb.puffy_to_long(coef, coef_idx={'coef_': 0, 'input_idcs': 0})
        coef['names'] = coef['model_name'].apply(rename)
        
        coef['rh'] = coef['input_idcs'].apply(lambda x: (OPSINS[int(x)] if (x >= 0) else int(x)))
        
        coefs[name] = coef
        
    coef_low = coefs['bs_coef'].groupby(
        list(set(coefs['coef'].columns) - set(['coef_']))
    )['coef_'].apply(
        np.percentile, q=CI_INTERVAL[0]
    ).reset_index().rename(columns={'coef_': 'coef_low'})
    coef_high = coefs['bs_coef'].groupby(
        list(set(coefs['coef'].columns) - set(['coef_']))
    )['coef_'].apply(
        np.percentile, q=CI_INTERVAL[1]
    ).reset_index().rename(columns={'coef_': 'coef_high'})
    
    coefs['coef'] = pd.merge(pd.merge(coefs['coef'], coef_low), coef_high)
    
    return data, {
        'r2s': r2s, 'scores': scores, 
        'r2ests': r2ests,
        'r2': r2_whole,
        'y_vs_pred': y_vs_pred, 
        'params': params, 
        'bs_params': bs_params, 
        # 'bs_data': bs_data, 
        **col_dict, 
        **coefs
    }
    

def get_encoding_model(key_model, model_like=None, postfix=''):
    data = (colormedulla.AllModelsCV() * colormedulla.ModelSettings()) & key_model
    if model_like is not None:
        data = data & model_like
    data = data.proj(
        f'final_model{postfix}'
    ).fetch(format='frame')[f'final_model{postfix}']
    data.name = data.name.replace(postfix, '')
    
    return data