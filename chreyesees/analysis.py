"""Dataset accumulation and main analysis
"""

from collections import defaultdict
from itertools import combinations, product
from sklearn.linear_model import LinearRegression

import numpy as np
from sklearn.preprocessing import PolynomialFeatures, Normalizer
from sklearn.pipeline import Pipeline
import inspect
from sklearn.decomposition import NMF
from scipy.spatial import ConvexHull
import pandas as pd
import datajoint as dj
import warnings
from sklearn import clone
from sklearn.metrics import pairwise_distances, r2_score
from dreye.api import spherical
from scipy.stats import norm
from dreye.api import convex, sampling, barycentric
from dreye.api import project
from scipy.spatial import ConvexHull
from sklearn.decomposition import TruncatedSVD
from sklearn.model_selection import RepeatedKFold
from sklearn.preprocessing import normalize
from tqdm import tqdm
from sklearn.preprocessing import minmax_scale

from chreyesees.utils import (
    calculate_sparsity, hist_angles,
    nan_r2_score, 
    load_bg_ints, load_bg_qs, load_bg_spectra, 
    load_receptor_estimator, 
    nan_r2er_score, 
    r2er_n2m, r2er_score, 
)
from chreyesees.nd_interpolate import CompleteMeRBFInterpolator
from chreyesees.pls import PLSRegression
from chreyesees import models, transform
from chreyesees.datajoint_attributes import pickletype  # used in declaration!
from chreyesees.db import schema
from chreyesees.processing import (
    CellName, CellTypes, # used in declaration
    Background, Recording, 
    Captures, Stats,
    SensitivitySet
)
from chreyesees.utils import rayleigh_value, rayleigh_vec, twod_transform

from . import (
    LEDS, OPSINS, QS, TBOOL_AMP, TBOOL_BASELINE, WLS
)

# thresholds to remove noisy ROIs
SNR_THRESH = 2

# clustering and meaning components
BINSIZE = np.pi / 2
LENGTH_PARAM = 0.5
SCALE_PARAM = 1.0
MIN_FRACTION = 0.1
BS_ITER = 1000
NONLINEAR_THRESH = (2, 98)  # initial threshold values for LNL model

CI_INTERVAL = (2.5, 97.5)

PEAKS = np.arange(320, 580, 1)
NON_SPECTRAL_PROPORTIONS = np.linspace(0, 1, 21)

def led_contrast(X, bg_ints):
    return X / np.maximum(bg_ints, 1) - 1

# recording conditions for compiling datasets
@schema
class RecordingCondition(dj.Manual):
    definition = """
    condition_name : varchar(63)
    ---
    restriction : longblob
    kwargs = null : longblob
    """

def populate_recording_condition():
    cell_restriction = (
        "(brain_area = 'lobula' AND cell_type LIKE 'Tm%') "
        "OR "
        "(brain_area = 'medulla' AND "
        "(cell_type LIKE 'pR%' "
        "OR cell_type LIKE 'yR%' "
        "OR cell_type LIKE 'pDm%' "
        "OR cell_type LIKE 'Dm%' "
        "OR cell_type LIKE 'yDm%' "
        "OR cell_type LIKE 'L3%' "
        "OR cell_type LIKE 'ML1%' "
        "OR cell_type LIKE 'Mi4%'))"
    )
    toinsert = [
        (
            'saline-main-all', [
                {'recording_solution': 'saline'}, 
                cell_restriction, 
                'est_snr > 2', 
            ], 
            None
        ),
        (
            'saline-gamut-all', [
                {'recording_solution': 'saline', 'stimulus_set': 'gamut'}, 
                cell_restriction, 
                'est_snr > 2', 
            ], 
            None
        ),
    ]

    RecordingCondition.insert(toinsert, skip_duplicates=True)
    
    # always update if restriction changes
    for row in toinsert:
        row = dict(zip(['condition_name', 'restriction', 'kwargs'], row))
        RecordingCondition.update1(row)



def _get_restricted_table(key, add_table=None,):
    conds = (RecordingCondition & key).fetch1('restriction')
    if isinstance(conds, (list, tuple)):
        conds = dj.AndList(conds)
    # recording_solution, subject_id, (genotype_id,) 
    # neuron_section, brain_area, bg_name, bg_int
    t_ = (
        CellTypes
        * Background.proj()
        * Recording.proj(
            # all these columns could be in the restriction
            'genotype_id', 'subject_id', 
            'recording_solution', 'neuron_section', 
            'brain_area', 'bg_name', 'bg_int'
        )
        * Recording.Events.proj(*LEDS)
        * Stats.Events.proj('amp', 'sig', 'psth')
        * Stats.Roi.proj('est_var', 'est_snr', 'auc')
    )
    table = (
        t_
        if add_table is None
        else t_ * add_table
    ) & dj.AndList([
        f"est_snr > {SNR_THRESH}",
    ]) & key & conds
    
    if len(table) == 0:
        print(f'No processed recordings yet skipping: {key}.')
        return None
    
    return table


def _get_table_and_df(key, add_table=None, extra_idcs=None):
    table = _get_restricted_table(key, add_table)
    if table is None:
        return None, None, None
    
    data = table.fetch(format='frame').reset_index()
    df = pd.pivot_table(
        data, 
        'amp', 
        (LEDS if extra_idcs is None else extra_idcs + LEDS), 
        ['stimulus_set', 'recording_id', 'cell_id']
    )
    return table, data, df


def _general_dataset_make_function(key, add_table=None, extra_idcs=None):
    """General function to spit out configured dataset for table entries
    """
    table, data, df = _get_table_and_df(key, add_table, extra_idcs)
    if table is None:
        return None, None, None, None, None
    bg_ints = load_bg_ints(key['bg_name'], key['bg_int'])
    
    # get psth array with nan filling
    df_psth = pd.pivot_table(
        data, 
        'psth', 
        (LEDS if extra_idcs is None else extra_idcs + LEDS), 
        ['stimulus_set', 'recording_id', 'cell_id'], 
        aggfunc=lambda x: np.mean(x, axis=0)
    )
    lengths = df_psth.applymap(len, na_action='ignore').fillna(0).astype(int).to_numpy()
    lengths[lengths == 0] = np.max(lengths)
    lengths = np.unique(lengths)
    assert len(lengths) == 1, f"More than one length: {lengths}!"
    length = lengths[0]
    psth = df_psth.to_numpy()
    for i, j in np.ndindex(psth.shape):
        if isinstance(psth[i, j], np.ndarray):
            continue
        if np.isnan(psth[i, j]):
            psth[i, j] = np.ones(length) * np.nan
        else:
            raise ValueError(f"Value is not NaN or numpy.ndarray: {psth[i, j]}")
    # LED indices x cell indices x time indices
    psth = np.array(psth.tolist())
    
    infos : pd.DataFrame = data.groupby(['stimulus_set', 'recording_id', 'cell_id'])[['est_var', 'est_snr', 'sig', 'auc']].mean()
    assert np.all(infos.index == df.columns)  # sanity check
    
    return table, df, psth, infos, bg_ints


def _pls_regression(key, X, Y, truncate_X=None, scale=False, demean=False):
    """
    Perform PLS regression, to decompose responses into bins to calculate the mean of later
    - make sure to only get neurons that are in the fullfield of view.
    """
    if truncate_X is not None:
        svd = TruncatedSVD(truncate_X)
        X = svd.fit_transform(X)
    
    key = key.copy()
    pls = PLSRegression(n_components=2, scale=scale, demean=demean)
    pls.fit(X, Y)
    
    # get orthogonal factors - removing bias feature
    rots, factors = pls.decompose_coef()
    if truncate_X is not None:
        rots = svd.inverse_transform(rots.T).T
    # get angles
    ang_factors = spherical.cartesian_to_spherical(factors)[:, 1]
    
    # mean = np.mean(factors, axis=0)
    # ang_mean = spherical.cartesian_to_spherical(mean[None])[0, 1]
    
    # calculate mode
    hist, bin_edges = np.histogram(ang_factors, bins=24, range=(0, 2*np.pi))
    ang_mean = np.mean([bin_edges[1:], bin_edges[:-1]], 0)[np.argmax(hist)]
    # ang_mean = circmean(ang_factors)
    mean = spherical.spherical_to_cartesian(np.array([1, ang_mean]))
    
    # 45 degree separation between response groups
    edge_centers = np.arange(
        # prevent floating point errors
        ang_mean, ang_mean + 2*np.pi - BINSIZE/2, BINSIZE
    ) % (2 * np.pi)
    # print(len(edge_centers))
    
    hist_bool = hist_angles(
        ang_factors, edge_centers, binsize=BINSIZE, stat=lambda x: x
    ).apply(pd.Series)
    labels = (
        hist_bool.to_numpy().astype(float) 
        * np.arange(hist_bool.shape[0])[:, None]
    ).sum(0)
    counts = hist_bool.to_numpy().sum(1)
    proportions = counts / np.sum(counts)
    
    edge_pairs = hist_bool.index.to_frame(False)[['e1', 'e2']].to_numpy()
    edge_centers = hist_bool.index.to_frame(False)['emean'].to_numpy()
    
    key['pls'] = pls
    key['rots'] = rots
    key['factors'] = factors
    key['mean_factor'] = mean
    key['angles'] = ang_factors
    key['mean_angle'] = ang_mean 
    key['labels'] = labels
    key['proportions'] = proportions
    key['edge_pairs'] = edge_pairs
    key['edge_centers'] = edge_centers
    return key


def _get_sigma(X, verbose=False):
    rng = np.random.default_rng(10)
    # Sigma (correlation) - add noise to prevent singularity for inverse calculation
    dists = pairwise_distances(X+rng.standard_normal(X.shape)*0.001)
    if verbose:
        print(f"pairwise distances describe: {np.mean(dists)}±{np.std(dists)}")
    Sigma = np.exp(-dists/LENGTH_PARAM) * SCALE_PARAM
    return Sigma


@schema
class AllDataset(dj.Computed):
    definition = """
    -> CellName
    -> Background
    -> RecordingCondition
    ---
    psth : longblob # psth across times
    inputs : longblob  # LED intensities (processed as intended)
    outputs : longblob  # response array (processed as intended)
    power : longblob # roi power
    var : longblob # roi variances (processed as intended)
    snr : longblob # roi SNRs
    sig : longblob # fraction of significant responses per roi
    auc : longblob # auc across stimuli
    info : longblob # roi information
    """


    def make(self, key):
        table, df, psth, infos, bg_ints = _general_dataset_make_function(
            key
        )
        if table is None:
            return
        
        condition_kwargs = (RecordingCondition & key).fetch1('kwargs')
        condition_kwargs = ({} if condition_kwargs is None else condition_kwargs)
        # subtract white point
        Y = df.to_numpy()
        # estimated power of responses
        ypower = np.nansum(Y**2, axis=0) / (np.sum(np.isfinite(Y), axis=0)-1)
        # scale by power of responses
        if condition_kwargs.get('normalize_dataset', True):
            Y = Y / np.sqrt(ypower)
            # n_stim x n_neurons x n_times
            psth = psth / np.sqrt(ypower[..., None])
        
        X = df.index.to_frame()[LEDS]
        X = led_contrast(X.to_numpy(), bg_ints)
        
        toinsert = {
            'psth': psth,
            'inputs': X,
            'outputs': Y,
            'power': ypower,
            'var': infos['est_var'].to_numpy() / ypower, 
            'snr': infos['est_snr'].to_numpy(), 
            'sig': infos['sig'].to_numpy(), 
            'auc': infos['auc'].to_numpy(),
            'info': infos.index.to_frame(False).to_numpy()
        }
        toinsert.update(key)
        
        self.insert1(toinsert)
            

@schema         
class AllCaptures(dj.Computed):
    definition = """
    -> AllDataset
    -> SensitivitySet
    ---
    captures : longblob
    """
    
    def make(self, key):
        print(key)
        Xleds, restriction = (AllDataset & key).fetch1('inputs', 'info')
        restriction = pd.DataFrame(
            restriction, columns=['stimulus_set', 'recording_id', 'cell_id']
        ).to_records()
        bg_ints = load_bg_ints(key['bg_name'], key['bg_int'])
        
        stable = (Recording.Events * (Captures.Events.proj(*QS) & key)) & restriction
        if len(stable) == 0:
            raise Exception(f"Captures not populated - {key}.")
        
        stable.proj(*QS, *LEDS)
        
        sdata = stable.fetch(format='frame').reset_index()
        
        sdf = sdata.groupby(
            LEDS
        )[QS].mean()
        
        # the transformation applied beforehand to compare if indexing was correct
        X = sdf.index.to_frame()[LEDS]
        X = led_contrast(X.to_numpy(), bg_ints)
        
        # TRUST IN ME - the grouping is the same as in AllDataset
        # sanity is not cheap
        assert np.allclose(X, Xleds), "index error!"
        
        bg_qs = load_bg_qs(key['bg_name'], key['bg_int'], key['set_name'])
        captures = sdf.to_numpy() / np.maximum(bg_qs, 1)
        key['captures'] = captures
        self.insert1(key)

            
@schema
class AllRegression(dj.Computed):
    definition = """
    -> AllCaptures
    ---
    pls : <pickletype>
    factors : longblob
    mean_factor : longblob
    rots : longblob
    angles : longblob
    mean_angle : double
    labels : longblob
    edge_pairs : longblob
    edge_centers : longblob
    proportions : longblob
    fraction : double
    """
    
    class GroupedMeans(dj.Part):
        definition = """
        -> master
        group_id : int
        ---
        counts : longblob
        bs_counts : longblob
        mean_psth : longblob
        psth_select : longblob
        psth_low : longblob
        psth_high : longblob
        psth_sd : longblob
        mean_outputs : longblob
        bs_means : longblob
        mean_est_var : double
        bs_est_var : longblob
        bs_low : longblob
        bs_high : longblob
        bs_sd : longblob
        allnan : longblob
        """
        
    def make(self, key):
        print(key)
        # use subtracted responses to group responses
        Y, snr = (AllDataset & key).fetch1('outputs', 'snr')
        # get relative capture values
        X = (AllCaptures & key).fetch1('captures')
        # calculate excitation value and scale excitation to range from -1 to 1
        X = transform.excitation_contrast(X) * 2
        
        condition_kwargs = (RecordingCondition & key).fetch1('kwargs')
        condition_kwargs = ({} if condition_kwargs is None else condition_kwargs)
        
        fraction = 1.0
        
        Y_ = Y.copy()
        Y_[np.isnan(Y_)] = 0.0
        
        toinsert = _pls_regression(key, X.copy(), Y_, truncate_X=None)
        labels = toinsert['labels']
        toinsert['fraction'] = fraction
        self.insert1(toinsert)
        
        # calculate stimulus covariance for MAP estimate of mean response
        # at each location
        Sigma = _get_sigma(X)
        M = np.linalg.inv(Sigma)
        
        Y, psth, var = (AllDataset & key).fetch1('outputs', 'psth', 'var')
        # Y: stim x neurons, psth: stim x neurons x time, var: neurons
        # subtract baseline response to account for amplitude calculation
        psth = psth - psth[..., TBOOL_BASELINE].mean(-1, keepdims=True)
        
        rng = np.random.default_rng(seed=101)
        for label in np.unique(labels):
            label_bool = labels == label
            if np.mean(label_bool) < MIN_FRACTION:
                continue
            
            # select labels
            Y_ = Y[:, label_bool]
            
            # calculate variance
            var_ = var[label_bool]
            stim_counts_ = np.isfinite(Y_).sum(0)
            mean_var_ = (np.sum(np.sqrt(var_) * stim_counts_)/np.sum(stim_counts_)) ** 2
            
            # where are the no stimuli for this group
            allnan = np.isnan(Y_).all(-1)
            if np.all(allnan):
                continue
            # y_ : neurons x stimuli, sigma_ : stimuli x stimuli
            y_ = Y_[~allnan].T
            M_ = M[~allnan][:, ~allnan]
            # counts : stimuli
            counts = np.isfinite(y_).sum(0)
            # psth: stimuli x neurons x time -> stimuli x time
            # selected stimuli and neurons
            psth_ = psth[:, label_bool][~allnan]
            # flip for mean calculation - time, neuron, stimuli -> (time x stimuli).T
            mean_psth = np.nanmean(psth_, axis=1)
            # stim x time - no smoothing here across stimulus space
            # stim
            ymean = np.nanmean(mean_psth[..., TBOOL_AMP], axis=-1)
            
            bs_means = []
            bs_vars = []
            bs_psths = []
            bs_counts = []
            for _ in range(BS_ITER):
                # redo calculation for bootstrapped iteratoins
                # bootstrap over neurons
                idcs = rng.integers(0, y_.shape[0], size=y_.shape[0])
                # n_neurons x n_stimuli -> n_stimuli x n_neurons
                Y__ = y_[idcs].T
                # remove allnan stimuli
                # n_stimuli
                allnan_ = np.isnan(Y__).all(-1)
                # empty slices
                if np.all(allnan_):
                    continue
                
                # count it all
                # n_stimuli
                counts_ = np.isfinite(Y__).sum(1)
                bs_counts.append(counts_)
                
                # calculate variance
                var__ = var_[idcs]
                # number of stimuli for each neuron
                stim_counts__ = np.isfinite(Y__).sum(0)
                # weighted mean 
                mean_var__ = (np.sum(np.sqrt(var__) * stim_counts__)/np.sum(stim_counts__)) ** 2
                bs_vars.append(mean_var__)
                
                # n_neurons x n_stimuli, n_stimuli x n_stimuli
                M__ = M_[~allnan_][:, ~allnan_]
                
                # calculate psths
                psth__ = psth_[:, idcs, :][~allnan_]
                # stimuli x time
                _mean_psth_ = np.nanmean(psth__, axis=1)
                
                mean_psth_ = np.ones(mean_psth.shape) * np.nan
                mean_psth_[~allnan_] = _mean_psth_
                
                ymean_ = np.nanmean(mean_psth_[..., TBOOL_AMP], axis=-1)
                
                # append bootstrapped mean
                bs_psths.append(mean_psth_)
                bs_means.append(ymean_)
                
            bs_low, bs_high = np.nanpercentile(bs_means, q=CI_INTERVAL, axis=0)
            bs_sd = np.sqrt(np.nanmean((ymean - np.array(bs_means))**2, 0))
            
            psth_low, psth_high = np.nanpercentile(bs_psths, q=CI_INTERVAL, axis=0)
            psth_sd = np.sqrt(np.nanmean((mean_psth - np.array(bs_psths))**2, 0))
                      
            toinsert = key.copy()
            toinsert['group_id'] = label
            toinsert['mean_psth'] = mean_psth
            toinsert['bs_counts'] = np.array(bs_counts)
            toinsert['psth_select'] = psth_
            toinsert['psth_low'] = psth_low
            toinsert['psth_high'] = psth_high
            toinsert['psth_sd'] = psth_sd
            toinsert['mean_outputs'] = ymean
            # bootstrapped distribution of mean -> AUC analysis
            toinsert['bs_means'] = np.array(bs_means)
            toinsert['mean_est_var'] = mean_var_
            toinsert['bs_est_var'] = np.array(bs_vars)
            toinsert['counts'] = counts
            toinsert['bs_low'] = bs_low
            toinsert['bs_high'] = bs_high
            toinsert['bs_sd'] = bs_sd
            toinsert['allnan'] = allnan
            
            self.GroupedMeans.insert1(toinsert)


@schema
class PairwiseCorrelation(dj.Computed):
    definition = """
    -> AllRegression.GroupedMeans
    ---
    time_corr_values : longblob
    time_corr_mean : longblob
    corr_values : longblob
    corr_mean : double
    """
    
    def make(self, key):
        # stimuli x neurons x time
        psth_select, counts = (AllRegression.GroupedMeans & key).fetch1(
            'psth_select', 'counts'
        )
        
        length = psth_select.shape[1]
        isfinite = np.isfinite(psth_select[..., 0])
        if not isfinite.sum(axis=1).mean() > 2:
            print("No shared responses")
            return
            # raise ValueError("No shared responses")
        rng = np.random.default_rng(11)
        r2s_time = []
        r2s = []
        for _ in range(BS_ITER):
            idcs = rng.permutation(length)
            idcs1 = idcs[:length//2]
            idcs2 = idcs[length//2:]
            
            psth1 = np.nanmean(psth_select[:, idcs1], axis=1)
            psth2 = np.nanmean(psth_select[:, idcs2], axis=1)
            amp1 = psth1[..., TBOOL_AMP].mean(-1)
            amp2 = psth2[..., TBOOL_AMP].mean(-1)
            
            # psth1 x psth2
            # psth1 * psth2
            r2s_time_ = nan_r2er_score(
                np.moveaxis(psth1, 0, -1), 
                np.moveaxis(psth2, 0, -1), 
                axis=-1, 
                counts=counts
            )
            r2s_ = nan_r2er_score(amp1, amp2, counts=counts)
            
            r2s_time.append(r2s_time_)
            r2s.append(r2s_)
            
        r2s_time = np.stack(r2s_time)
        r2s = np.stack(r2s)
        
        key['time_corr_values'] = r2s_time
        key['time_corr_mean'] = np.nanmean(r2s_time, axis=0)
        key['corr_values'] = r2s
        key['corr_mean'] = np.nanmean(r2s)
        self.insert1(key)


@schema 
class SparsityIndex(dj.Computed):
    definition = """
    -> AllRegression.GroupedMeans
    ---
    auc : double
    auc_low : double
    auc_high : double
    bs_auc : longblob
    percentiles : longblob
    ordered_r : longblob
    ordered_r_low : longblob
    ordered_r_high : longblob
    bs_ordered_r : longblob
    """
    
    def make(self, key):
        # stim set types
        ymean, bs_means, counts, bs_counts = (
            AllRegression.GroupedMeans & key
        ).fetch1(f'mean_outputs', f'bs_means', 'counts', 'bs_counts')
        
        ymean = ymean[counts > 5]
        bs_means = bs_means.copy()
        bs_means[bs_counts <= 5] = np.nan
        
        # percentiles, y, auc = calculate_sparsity(ymean, remove_outliers=95)
        # _, y_bs, auc_bs = calculate_sparsity(bs_means, remove_outliers=95)
        percentiles, y, auc = calculate_sparsity(ymean, remove_outliers=None)
        _, y_bs, auc_bs = calculate_sparsity(bs_means, remove_outliers=None)
        
        auc_low, auc_high = np.nanpercentile(auc_bs, q=CI_INTERVAL, axis=0)
        y_low, y_high = np.nanpercentile(y_bs, q=CI_INTERVAL, axis=0)
        
        key[f'auc'] = auc
        key[f'auc_low'] = auc_low
        key[f'auc_high'] = auc_high
        key[f'bs_auc'] = auc_bs
        key[f'percentiles'] = percentiles
        key[f'ordered_r'] = y
        key[f'ordered_r_low'] = y_low
        key[f'ordered_r_high'] = y_high
        key[f'bs_ordered_r'] = y_bs
        self.insert1(key)

        
@schema
class InvarianceSettings(dj.Manual):
    definition = """
    inv_settings_name : varchar(31)
    ---
    idcs : longblob
    xtype : enum('q', 'l')
    transform : varchar(31)
    method : varchar(63)
    """
    
InvarianceSettings.insert([
    ['log_contrast', [1, 2, 3, 4], 'q', 'log_contrast', 'all'],
], skip_duplicates=True)


def _invariance_helper(X, y, method, counts):
    isfinite = np.isfinite(y)
    X, y, counts = X[isfinite], y[isfinite], counts[isfinite]
    if 'all' in method:
        Xinv, X = X[:, :1], X 
    else:
        Xinv, X = X[:, :1], X[:, 1:]
        
    if 'intercept' in method:
        fit_intercept = True
    else:
        fit_intercept = False
        
    model = LinearRegression(fit_intercept=fit_intercept)
    ypred = model.fit(X, y, sample_weight=counts).predict(X)
    # score = r2_score(y, ypred, sample_weight=counts)
    score = r2er_score(y, ypred, counts=counts)
    ypred = model.fit(Xinv, y, sample_weight=counts).predict(Xinv)
    # scoreinv = r2_score(y, ypred, sample_weight=counts)
    scoreinv = r2er_score(y, ypred, counts=counts)
    return np.abs(scoreinv / score)


@schema 
class InvarianceIndex(dj.Computed):
    definition = """
    -> AllRegression.GroupedMeans
    -> InvarianceSettings
    ---
    invariance : double
    bs_invariance : longblob
    """
    
    @property
    def key_source(self):
        # it takes time so restrict to group_id 0
        return super().key_source & {'group_id': 0}
    
    def make(self, key):
        # stim set types
        nans = (
            AllRegression.GroupedMeans & key
        ).fetch1('allnan')
        Q = (AllCaptures & key).fetch1('captures')
        Q = Q[~nans]
        X = (AllDataset() & key).fetch1('inputs')
        X = X[~nans]
        
        settings = (InvarianceSettings & key).fetch1()
        if settings['xtype'] == 'q':
            X = Q
        else:
            # add back one subtraction from before if LEDS
            X = X+1
        
        transform_func = getattr(transform, settings['transform'])  
        X = transform_func(X)
        
        idcs = settings['idcs']
        X = X[:, idcs].copy()
        
        method = settings['method']
        ymean, bs_means, counts, bs_counts = (
            AllRegression.GroupedMeans & key
        ).fetch1(
            f'mean_outputs', 
            f'bs_means', 
            'counts', 
            'bs_counts'
        )

        X_ = X
            
        A = transform.bigA
        X_ = X_ @ A
        
        # make data symmetric
        X_ = np.vstack([X_, -X_])
        ymean = np.concatenate([ymean, -ymean], axis=-1)
        counts = np.concatenate([counts, counts])
        bs_means = np.concatenate([bs_means, -bs_means], axis=-1)
        bs_counts = np.concatenate([bs_counts, bs_counts], axis=-1)
        
        invariance = _invariance_helper(X_, ymean, method, counts)
        bs_invariance = np.array([
            _invariance_helper(X_, y, method, count) 
            for y, count in zip(bs_means, bs_counts)
        ])
        
        key[f'invariance'] = invariance
        key[f'bs_invariance'] = bs_invariance
            
        self.insert1(key)  
        
        
@schema 
class InvarianceIndex2(dj.Computed):
    definition = """
    -> AllRegression.GroupedMeans
    ---
    invariance : double
    bs_invariance : longblob
    """
    
    @property
    def key_source(self):
        # it takes time so restrict to group_id 0
        return super().key_source & {'group_id': 0}
    
    def make(self, key):
        # stim set types
        nans = (
            AllRegression.GroupedMeans & key
        ).fetch1('allnan')
        Q = (AllCaptures & key).fetch1('captures')
        Q = Q[~nans]
        
        X = Q[:, [1, 2, 3, 4]].copy()  
        X = transform.log_pca(X, thresh=1e-3)
        Xchr = X[:, 1:]
        
        def helper_function(X, y, w):
            finites = np.isfinite(y)
            model = LinearRegression(fit_intercept=False)
            model.fit(X[finites], y[finites], sample_weight=w[finites])
            ypred = model.predict(X)
            return r2er_score(y[finites], ypred[finites], counts=w[finites]) + 1
              
        ymean, bs_means, counts, bs_counts = (
            AllRegression.GroupedMeans & key
        ).fetch1(
            f'mean_outputs', 
            f'bs_means', 
            'counts', 
            'bs_counts'
        )            
        
        invariance = helper_function(Xchr, ymean, counts)
        bs_invariance = np.array([
            helper_function(Xchr, y, count) 
            for y, count in zip(bs_means, bs_counts)
        ])
        
        key[f'invariance'] = invariance
        key[f'bs_invariance'] = bs_invariance
            
        self.insert1(key)  

        
@schema 
class RayleighIndex(dj.Computed):
    definition = """
    -> AllRegression.GroupedMeans
    ---
    rayleigh = null : double
    rayleigh_vec = null : longblob
    rayleigh_bs = null : longblob
    rayleigh_bs_vec = null : longblob
    """
    
    @property
    def key_source(self):
        # it takes time so restrict to group_id 0
        return super().key_source & {'group_id': 0}
    
    def make(self, key):
        # stim set types
        nans = (
            AllRegression.GroupedMeans & key
        ).fetch1('allnan')
        Q = (AllCaptures & key).fetch1('captures')
        Q = Q[~nans]
        
        
        # only take rh3, 4, 5, 6
        X = twod_transform(Q[:, [1, 2, 3, 4]])
        
        ymean, bs_means, counts, bs_counts = (
            AllRegression.GroupedMeans & key
        ).fetch1(
            f'mean_outputs', 
            f'bs_means', 
            'counts', 
            'bs_counts'
        )
        
        R_bar = rayleigh_value(counts, X, ymean)

        R_bar_bs = np.array([
            rayleigh_value(c, X, y)
            for c, y in zip(bs_counts, bs_means)
        ])
        
        R_bar_vec = rayleigh_vec(counts, X, ymean)

        R_bar_bs_vec = np.array([
            rayleigh_vec(c, X, y)
            for c, y in zip(bs_counts, bs_means)
        ])
        
        key[f'rayleigh'] = R_bar
        key[f'rayleigh_bs'] = R_bar_bs
        
        key[f'rayleigh_vec'] = R_bar_vec
        key[f'rayleigh_bs_vec'] = R_bar_bs_vec
            
        self.insert1(key)


@schema      
class ModelSettings(dj.Manual):
    definition = """
    model_name : varchar(63)
    ---
    input_idcs : longblob
    transform_func : varchar(63)
    transform_kwargs : longblob
    class_name : varchar(63)
    init_kwargs : longblob
    save_params : longblob
    data_restriction : varchar(63)
    """
    

def populate_model_settings():
    
    ModelSettings.insert([
        (
            'rbf-log',  # name
            [1, 2, 3, 4],  # input_idcs
            'log_contrast',  # transform_func
            {'thresh': 1e-3},  # transfrom_kwargs
            'RbfModel',  # class_name 
            {'normalize': False, 'inhull': False, 'bias': False, 'demeaned': True},  # init_kwargs
            [],  # save_params
            'all',  # data_restriction
        ),
        (
            'linear-log',  # name
            [1, 2, 3, 4],  # input_idcs
            'log_contrast',  # transform_func
            {'thresh': 1e-3},  # transfrom_kwargs
            'LinearRegression',  # class_name 
            {'fit_intercept': False},  # init_kwargs
            ['coef_', 'intercept_'],  # save_params
            'all',  # data_restriction
        ),
        (
            'linear-log-rh1',  # name
            [0, 1, 2, 3, 4],  # input_idcs
            'log_contrast',  # transform_func
            {'thresh': 1e-3},  # transfrom_kwargs
            'LinearRegression',  # class_name 
            {'fit_intercept': False},  # init_kwargs
            ['coef_', 'intercept_'],  # save_params
            'all',  # data_restriction
        ),
        (
            'lnl-log',  # name
            [1, 2, 3, 4],  # input_idcs
            'log_contrast',  # transform_func
            {'thresh': 1e-3},  # transfrom_kwargs
            'ThresholdLinear',  # class_name 
            {
                'fit_intercept': False, 'threshold_y': NONLINEAR_THRESH,
                'tanh': True, 'refit': 'threshold'
                # 'refit': 'all',
            },  # init_kwargs
            ['coef_', 'intercept_', 'ylower_', 'yupper_'],  # save_params
            'all',  # data_restriction
        ),
        (
            'lnl-log-rh1',  # name
            [0, 1, 2, 3, 4],  # input_idcs
            'log_contrast',  # transform_func
            {'thresh': 1e-3},  # transfrom_kwargs
            'ThresholdLinear',  # class_name 
            {
                'fit_intercept': False, 'threshold_y': NONLINEAR_THRESH,
                'tanh': True, 'refit': 'threshold'
                # 'refit': 'all',
            },  # init_kwargs
            ['coef_', 'intercept_', 'ylower_', 'yupper_'],  # save_params
            'all',  # data_restriction
        ),
        (
            'mises-log',  # name
            [1, 2, 3, 4],  # input_idcs
            'log_pca',  # transform_func
            {'thresh': 1e-3},  # transfrom_kwargs
            'SelectivityModel',  # class_name 
            {
                'linear_features': 0, 
            },  # init_kwargs
            ['coef_', 'intercept_', 'tau_', 'alpha_', 'kappa_'],  # save_params
            'all',  # data_restriction
        ),
    ], skip_duplicates=True)


@schema           
class AllModelsCV(dj.Computed):
    definition = """
    -> AllRegression.GroupedMeans
    -> ModelSettings
    ---
    inputs : longblob
    outputs : longblob
    bs_outputs : longblob
    counts : longblob
    bs_counts : longblob
    scores : longblob
    r2s : longblob
    r2ests : longblob
    r2 : double  # complete r2
    params : longblob
    models : <pickletype>
    final_model : <pickletype>
    final_params : longblob
    outputs_pred : longblob
    """
    
    n_repeats = 4
    n_splits = 4
    random_state = 11
        
    @property
    def key_source(self):
        return super().key_source & {'group_id': 0}
    
    def make(self, key):
        print(f"populating {key}")
        # initialize insertion
        toinsert = key.copy()
        # toinsert_bs = []
        
        model_settings = (ModelSettings & key).fetch1()
        
        X = (AllCaptures & key).fetch1('captures')
        allnan, counts, bs_counts = (AllRegression.GroupedMeans & key).fetch1(
            'allnan', 'counts', 'bs_counts'
        )
        X = X[~allnan]
        idcs = model_settings['input_idcs']
        X = X[:, idcs].copy()
        inhull = None
            
        # transform X points
        func = getattr(transform, model_settings['transform_func'])
        X = func(X, **model_settings['transform_kwargs'])
        
        toinsert['inputs'] = X
        toinsert['counts'] = counts
        toinsert['bs_counts'] = bs_counts
        
        # get model and cv object
        model = getattr(models, model_settings['class_name'])(
            **model_settings['init_kwargs']
        )
        
        rkf = RepeatedKFold(
            n_repeats=self.n_repeats, 
            n_splits=self.n_splits, 
            random_state=self.random_state
        )
            
        y, bs_sd, bs_y = (AllRegression.GroupedMeans & key).fetch1(
            f'mean_outputs', f'bs_sd', f'bs_means'
        ) 
        if inhull is not None:
            y = y[inhull]
            bs_sd = bs_sd[inhull]
            bs_y = bs_y[..., inhull]
            
        X_ = X
        counts_ = counts

        bs_var = bs_sd ** 2
        
        toinsert[f'outputs'] = y
        toinsert[f'bs_outputs'] = bs_y
                
        # cross-validation
        print(f"running cross-validation for ``")
        results = defaultdict(list)
        for train_idx, test_idx in rkf.split(X_):
            Xtrain = X_[train_idx]
            Xtest = X_[test_idx]
            ytrain = y[train_idx]
            ytest = y[test_idx]
            
            # clone and train model
            model_ = clone(model)
            model_.fit(Xtrain, ytrain, sample_weight=counts_[train_idx])
            ypred = model_.predict(Xtest)
            
            # average variance and counts for test set
            est_var = np.sum(bs_var[test_idx] * counts_[test_idx])/np.sum(counts_[test_idx])
            
            results['test_r2'].append(nan_r2_score(ytest, ypred, sample_weight=counts_[test_idx]))
            results['test_r2er'].append(nan_r2er_score(ytest, ypred, counts=counts_[test_idx]))
            results['test_r2er_est'].append(
                r2er_n2m(ytest, ypred, var=est_var, n=counts_[test_idx])
            )
            results['estimator'].append(model_)

        toinsert[f'scores'] = results['test_r2']
        toinsert[f'r2s'] = results['test_r2er']
        toinsert[f'r2ests'] = results['test_r2er_est']
                    
        # saving of attributes
        params = {}
        for attr in model_settings['save_params']:
            params[attr] = []
            for est in results['estimator']:
                params[attr].append(getattr(est, attr))
            params[attr] = np.array(params[attr])
        
        toinsert[f'params'] = params
        toinsert[f'models'] = results['estimator']
        
        # fit final model for direct comparison
        model.fit(X_, y, sample_weight=counts_)
        
        ypred = model.predict(X_)
        
        toinsert[f'r2'] = nan_r2_score(y, ypred, sample_weight=counts_)
        toinsert[f'final_model'] = model
        toinsert[f'outputs_pred'] = model.predict(X_)
        toinsert[f'final_params'] = {
            attr : getattr(model, attr)
            for attr in model_settings['save_params']
        }
        
        self.insert1(toinsert)


@schema     
class BoostrappedModels(dj.Computed):
    definition = """
    -> AllModelsCV
    """
    
    class Bootstrap(dj.Part):
        definition = """
        -> master
        boot_idx : int
        ---
        # bs_params : longblob
        bs_model : <pickletype>
        r2 : double
        """
        
    def make(self, key):
        X, counts, bs_counts, model = (AllModelsCV & key).fetch1('inputs', 'counts', 'bs_counts', 'final_model')
        toinsert_bs = []
        skipped_bs = []
        # bs_y, bs_counts, counts, X, model
        bs_y = (AllModelsCV & key).fetch1(f'bs_outputs')
        print(f"running bootstrap ``")
        for idx, (y_, counts_) in enumerate(zip(bs_y, bs_counts)):
            isfinite = np.isfinite(y_)
            if np.sum(isfinite) < 10:
                skipped_bs.append(idx)
                continue
            
            assert idx not in skipped_bs, "idx in previously skipped bootstrap iteration."
            
            y_ = y_[isfinite]
            X_ = X[isfinite]
            counts_ = counts[isfinite]
            
            # print(type(model))
            model = clone(model)
            model.fit(X_, y_, sample_weight=counts_)
            ypred = model.predict(X_)
            
            toinsert_bs_ = key.copy()
            toinsert_bs_[f'boot_idx'] = idx
            toinsert_bs_[f'bs_model'] = model
            toinsert_bs_[f'r2'] = nan_r2_score(y_, ypred, sample_weight=counts_)
            
            if len(toinsert_bs) > idx:
                toinsert_bs[idx].update(toinsert_bs_)
            else:
                toinsert_bs.append(toinsert_bs_)
    
        self.insert1(key)
        self.Bootstrap.insert(toinsert_bs)


def get_surface_proportions(proportions=NON_SPECTRAL_PROPORTIONS):
    props = []
    for iidx, iprop in enumerate(proportions):
        for jprop in proportions[:len(proportions)-iidx]:
            kprop = 1 - (iprop + jprop)
            assert np.isclose(sum([kprop, iprop, jprop]), 1)
            props.append([iprop, jprop, kprop])
    return np.array(props)


def _reassign_X(X, trained_X, proj_method='argmin'):
    if proj_method == 'argmin':
        argmins = np.argmin(
            np.linalg.norm(X - trained_X[:, None, :], axis=-1), 
            axis=0
        )
        # sanity check
        assert len(argmins) == len(X), "BUG"
        X = trained_X[argmins]
    elif proj_method == 'hull':
        equations = ConvexHull(trained_X).equations
        X = project.proj_B_to_hull(X, equations)
    else:
        raise ValueError(f"unknown proj_method: {proj_method}")
    return X
    

def calculate_wavelength_X(
    multiple, mult_type, std, 
    set_name, bg_name, bg_int, 
    q_idcs, transform_func, 
    peaks=PEAKS, 
    add_background=False, transform_kwargs={}, 
    non_spectral_lines=False, 
    proportions=NON_SPECTRAL_PROPORTIONS, 
    closest=False, trained_X=None, 
    proj_method='argmin', 
):
    exclude_opsins = list(set(OPSINS) - set(np.array(OPSINS)[q_idcs]))
    
    est = load_receptor_estimator(
        set_name, bg_name, bg_int, exclude_opsins=exclude_opsins
    )
    
    single_wls = norm.pdf(WLS[None], loc=peaks[:, None], scale=std)
    if mult_type == 'isoflux':
        single_wls = single_wls * multiple
    elif mult_type == 'bg':
        single_wls = single_wls * bg_int
        
    if add_background:
        bg_spectra = load_bg_spectra(bg_name, bg_int)
        single_wls = single_wls + bg_spectra
    
    q = est.relative_capture(single_wls)
    qnorm = q / np.maximum(np.sum(q, axis=-1, keepdims=True), 1e-8)
    if mult_type == 'isoluminant':
        # on average it is one
        q = (  # make all q sum up to one / n
            q 
            / np.maximum(np.mean(q, axis=-1, keepdims=True), 1e-8)
        ) * multiple
        
    transform_func = getattr(transform, transform_func)
    X = transform_func(q, **transform_kwargs)
    
    if closest:
        assert trained_X is not None
        X = _reassign_X(X, trained_X, proj_method=proj_method)
        
        
    if non_spectral_lines:
        # qnorm is sorted according to peaks
        # locations of peak relative excitation
        argmax = np.argmax(qnorm, axis=0)
        non_spectral_dict = {}
        # go over indices for argmax
        for idx, jdx in combinations(
            range(argmax.size), 2
        ):
            # skip adjacent receptors?
            # if np.abs(idx - jdx) <= 1:
            #     continue
            iidx = argmax[idx]
            jjdx = argmax[jdx]
            non_spectral_dict[
                (peaks[idx], peaks[jdx], est.labels[idx], est.labels[jdx])
            ] = transform_func(
                (
                    proportions[:, None] * q[iidx]
                    +
                    (1 - proportions)[:, None] * q[jjdx]
                ), **transform_kwargs
            )
            
        surface_dict = {}
        surface_props = get_surface_proportions(proportions)
        # get proportions
        for idx, jdx, kdx in combinations(range(argmax.size), 3):
            iidx = argmax[idx]
            jjdx = argmax[jdx]
            kkdx = argmax[kdx]
            surface_dict[(
                est.labels[idx], est.labels[jdx], est.labels[kdx]
            )] = transform_func(
                (
                    surface_props[:, 0, None] * q[iidx]
                    +
                    surface_props[:, 1, None] * q[jjdx]
                    +
                    surface_props[:, 2, None] * q[kkdx]
                ), **transform_kwargs
            )
            
        if closest:
            for ledict in [non_spectral_dict, surface_dict]:
                for k, x in ledict.items():
                    ledict[k] = _reassign_X(x, trained_X, proj_method=proj_method)

        return X, non_spectral_dict, surface_dict
    
    return X


@schema
class LambdaLineSettings(dj.Manual):
    definition = """
    line_settings : varchar(32)
    ---
    multiple : double
    mult_type : enum('isoluminant', 'isoflux', 'bg')
    std : double
    add_background : tinyint
    kwargs : longblob
    """
    

def populate_lambda_settings():
    LambdaLineSettings.insert([
        ('gamut-intensity-proj', 5, 'isoluminant', 20, False, {'closest': True, 'proj_method': 'hull'}), 
    ], skip_duplicates=True)
    
    
def _line_fits(key, X):
    key = key.copy()
    model, cv_models = (AllModelsCV & key).fetch1(f'final_model', f'models')
    pkws = {}
    if 'do_clip' in inspect.signature(model.predict).parameters:
        pkws['do_clip'] = False
    if 'ignore_hull' in inspect.signature(model.predict).parameters:
        pkws['ignore_hull'] = True
    tuning = model.predict(X, **pkws)
    key['prediction'] = tuning
    key['inputs'] = X
    
    bs_predictions = []
    for model in cv_models:
        tuning = model.predict(X, **pkws)
        bs_predictions.append(tuning)
        
    key['bs_prediction'] = np.array(bs_predictions)
    key['bs_low'], key['bs_high'] = np.nanpercentile(bs_predictions, q=CI_INTERVAL, axis=0)
    return key
    

@schema
class LambdaLinePrediction(dj.Computed):
    definition = """
    -> AllModelsCV
    -> LambdaLineSettings
    ---
    inputs : longblob
    prediction : longblob
    bs_prediction : longblob
    bs_low : longblob
    bs_high : longblob
    """
    
    class NonSpectrals(dj.Part):
        definition = """
        -> master
        nonspectral_name : varchar(31)
        ---
        peak1 : double
        peak2 : double
        inputs : longblob
        prediction : longblob
        bs_prediction : longblob
        bs_low : longblob
        bs_high : longblob
        """
        
    class Surfaces(dj.Part):
        definition = """
        -> master
        surface_name : varchar(31)
        ---
        inputs : longblob
        prediction : longblob
        bs_prediction : longblob
        bs_low : longblob
        bs_high : longblob
        """
    
    def make(self, key):
        print(key)
        warnings.filterwarnings("ignore", category=UserWarning)
        multiple, mult_type, std, add_background, kwargs = (LambdaLineSettings & key).fetch1(
            'multiple', 'mult_type', 'std', 'add_background', 'kwargs'
        )
        settings = (ModelSettings & key).fetch1()
        
        trained_X = (AllModelsCV & key).fetch1('inputs')
             
        X, non_spectral_dict, surface_dict = calculate_wavelength_X(
            multiple, 
            mult_type, std, 
            key['set_name'], key['bg_name'], 
            key['bg_int'], 
            settings['input_idcs'], 
            transform_func=settings['transform_func'], 
            add_background=add_background, 
            non_spectral_lines=True, 
            trained_X=trained_X, 
            closest=kwargs.get('closest', False), 
            proj_method=kwargs.get('proj_method', 'argmin')
        )
        
        toinsert = _line_fits(key, X)
        self.insert1(toinsert)
        
        for (peak1, peak2, opsin1, opsin2), X in non_spectral_dict.items():
            toinsert = _line_fits(key, X)
            toinsert['nonspectral_name'] = f"{opsin1}-{opsin2}"
            toinsert['peak1'] = peak1
            toinsert['peak2'] = peak2
            self.NonSpectrals.insert1(toinsert)
            
        for (opsin1, opsin2, opsin3), X in surface_dict.items():
            toinsert = _line_fits(key, X)
            toinsert['surface_name'] = f"{opsin1}-{opsin2}-{opsin3}"
            self.Surfaces.insert1(toinsert)
      

def _bs_line_fits(key, X):
    key = key.copy()
    model = (BoostrappedModels.Bootstrap & key).fetch1(f'bs_model')
    pkws = {}
    if 'do_clip' in inspect.signature(model.predict).parameters:
        pkws['do_clip'] = False
    if 'ignore_hull' in inspect.signature(model.predict).parameters:
        pkws['ignore_hull'] = True
    tuning = model.predict(X, **pkws)
    key['prediction'] = tuning
    key['inputs'] = X
    return key      

@schema
class BsLambdaLinePrediction(dj.Computed):
    definition = """
    -> BoostrappedModels.Bootstrap
    -> LambdaLineSettings
    ---
    inputs : longblob
    prediction : longblob
    """
    
    class NonSpectrals(dj.Part):
        definition = """
        -> master
        nonspectral_name : varchar(31)
        ---
        peak1 : double
        peak2 : double
        inputs : longblob
        prediction : longblob
        """
        
    class Surfaces(dj.Part):
        definition = """
        -> master
        surface_name : varchar(31)
        ---
        inputs : longblob
        prediction : longblob
        """
        
    def make(self, key):
        print(key)
        warnings.filterwarnings("ignore", category=UserWarning)
        multiple, mult_type, std, add_background, kwargs = (LambdaLineSettings & key).fetch1(
            'multiple', 'mult_type', 'std', 'add_background', 'kwargs'
        )
        settings = (ModelSettings & key).fetch1()
        
        trained_X = (AllModelsCV & key).fetch1('inputs')
             
        X, non_spectral_dict, surface_dict = calculate_wavelength_X(
            multiple, 
            mult_type, std, 
            key['set_name'], key['bg_name'], 
            key['bg_int'], 
            settings['input_idcs'], 
            transform_func=settings['transform_func'], 
            add_background=add_background, 
            non_spectral_lines=True, 
            trained_X=trained_X, 
            closest=kwargs.get('closest', False), 
            proj_method=kwargs.get('proj_method', 'argmin')
        )
        
        toinsert = _bs_line_fits(key, X)
        self.insert1(toinsert)
        
        for (peak1, peak2, opsin1, opsin2), X in non_spectral_dict.items():
            toinsert = _bs_line_fits(key, X)
            toinsert['nonspectral_name'] = f"{opsin1}-{opsin2}"
            toinsert['peak1'] = peak1
            toinsert['peak2'] = peak2
            self.NonSpectrals.insert1(toinsert)
            
        for (opsin1, opsin2, opsin3), X in surface_dict.items():
            toinsert = _bs_line_fits(key, X)
            toinsert['surface_name'] = f"{opsin1}-{opsin2}-{opsin3}"
            self.Surfaces.insert1(toinsert)