from chreyesees import plot_utils
import seaborn as sns
import numpy as np
import proplot as pplt
from itertools import product
from chreyesees.palettes import CMAP_CELLS
from chreyesees.plot_tetrahedron import plot_tetrahedron

RADIUS = 0.1
WIGGLE = 1
TRANSFORM_FUNC = lambda x: x
POINT_SIZE = 3
POINT_OFFSET = 3.5

def sparsity_helper(ax, df_sparsity, cells, add_ylabel=True):
    for _, row in df_sparsity.iterrows():
        if row['cell_type'] not in cells:
            continue
        ax.fill_betweenx(
            row['percentiles'], 
            row['ordered_r_low'], 
            row['ordered_r_high'], 
            alpha=0.5, 
            color=CMAP_CELLS[row['cell_type']]
        )
        ax.plot(
            row['ordered_r'], 
            row['percentiles'], 
            label=row['cell_type'], 
            color=CMAP_CELLS[row['cell_type']]
        )
        
    handles, labels = ax.get_legend_handles_labels()
    ax.set_xlabel('rel. abs. response (a.u.)')
    if add_ylabel:
        ax.set_ylabel('cumulative proportion')
    ax.set_ylim(0, 1)
    ax.set_xlim(0, 1)
    plot_utils.add_identity(axes=ax, linestyle='--', alpha=0.5, color='gray')
    sns.despine(ax=ax)
    return handles, labels


def sparsity_sum_plot(ax, data_sparsity, cell_types, arange=(0.3, 1)):
    plot_utils.vertical_histogram(
        data=data_sparsity, 
        x='cell_type', 
        y='bs_auc', 
        # mean='auc',  
        order=cell_types, 
        ax=ax, 
        arange=arange,
        bins=int((arange[1]-arange[0])*10*3), 
        hue='cell_type',
        # color='gray', 
        palette_dict=CMAP_CELLS,
        alpha=0.5,
        # mean_kwargs={'color': 'black'}
    )
    ax.axhline(0.5, linestyle='--', color='gray', alpha=0.5)
    ax.set_xticklabels(ax.get_xticklabels(), rotation = 90)
    for ticklabel in ax.get_xticklabels():
        tickcolor = CMAP_CELLS[ticklabel.get_text()]
        ticklabel.set_color(tickcolor)
    ax.set_ylim(*arange)
    ax.set_xlabel(None)
    ax.set_ylabel('sparsity index')
    sns.despine(ax=ax)
    return ax


def invariance_plot(ax, inv_df, cell_types, reverse=True):
    if reverse:
        inv_df['bs_invariance_rev'] = (1/inv_df['bs_invariance'])
        name = 'bs_invariance_rev'
    else:
        name = 'bs_invariance'
    maxi = np.ceil(inv_df[name].abs().max())
    if reverse:
        # arange = (-maxi, maxi)
        arange = (0, 2)
        # arange = (0, np.maximum(1.5, maxi))
    else:
        arange = (0, np.maximum(4, maxi))
    plot_utils.vertical_histogram(
        data=inv_df, 
        x='cell_type', 
        y=name, 
        # mean='invariance', 
        bins=int((arange[1]-arange[0])*3), 
        hue='cell_type', 
        palette_dict=CMAP_CELLS, 
        order=cell_types, 
        arange=arange, 
        ax=ax, 
        alpha=0.5,
    )
    if reverse:
        ax.axhline(
            1, color='gray', 
            alpha=0.5, 
            linestyle='--'
        )
    else:
        ax.axhline(
            1, color='gray', 
            alpha=0.5, 
            linestyle='--'
        )
    if reverse:
        ax.set_ylim(0, 2)
        # ax.set_ylim(-maxi, maxi)
    else:
        ax.set_ylim(0, np.maximum(4, maxi))
    ax.set_xticklabels(ax.get_xticklabels(), rotation = 90)
    for ticklabel in ax.get_xticklabels():
        tickcolor = CMAP_CELLS[ticklabel.get_text()]
        ticklabel.set_color(tickcolor)
    ax.set_xlabel(None)
    if reverse:
        ax.set_ylabel('chromatic\nvariance index')
    else:
        ax.set_ylabel('achromatic\nvariance index')
    sns.despine(ax=ax)
    return ax


def rayleigh_plot(ax, inv_df, cell_types):
    name = 'rayleigh_bs_hard'
    arange = (0, 1)
    plot_utils.vertical_histogram(
        data=inv_df, 
        x='cell_type', 
        y=name, 
        # mean='invariance', 
        bins=20, 
        hue='cell_type', 
        palette_dict=CMAP_CELLS, 
        order=cell_types, 
        arange=arange, ax=ax, 
        alpha=0.5,
    )
    # ax.axhline(
    #     1, color='gray', 
    #     alpha=0.5, 
    #     linestyle='--'
    # )
    ax.set_ylim(0, 1)
    ax.set_xticklabels(ax.get_xticklabels(), rotation = 90)
    for ticklabel in ax.get_xticklabels():
        tickcolor = CMAP_CELLS[ticklabel.get_text()]
        ticklabel.set_color(tickcolor)
    ax.set_xlabel(None)
    ax.set_ylabel('rayleigh index')
    sns.despine(ax=ax)
    return ax


def create_tetrahedron_panels(
    gridspec, 
    fig, 
    axs, 
    total_panels, 
    start_row=0,
    ncols=4, 
    number_of_tetrahedra=8, 
    number_start=1, 
    panel_numbers=None
):
    axes = []
    axins = []
    for i in range(number_of_tetrahedra):
        irow = i//ncols * 2 + start_row
        icol = i % ncols
        if panel_numbers is None:
            number = i + number_start
        else:
            number = panel_numbers[i]
        ax = fig.add_subplot(
            gridspec[irow, icol*3:(icol+1)*3],
            proj='3d', 
            number=number
        )
        axs.append(ax)
        axin = []
        for j in range(3):
            axin_ = fig.add_subplot(
                gridspec[irow+1, icol*3+j],
                number=total_panels - i * 3 + j,
            )
            axin.append(axin_)
        axins.append(axin)
        axes.append(ax)
    return axes, axins


def plot_tetra_data(
    tetra_data, cell_types, 
    axes, axins, est, 
    radius, fontsize=9, 
    point_size=POINT_SIZE, 
    point_offset=POINT_OFFSET
):
    for name, data in tetra_data.groupby('cell_type'):
        if name not in cell_types:
            continue
        index = cell_types.index(name)
        ax = axes[index]
        axin = axins[index]
        
        r = data['r'].to_numpy()
        psth = np.stack(data['psth_rois'].to_numpy())
        Q = data[est.labels].to_numpy()
        ax.text(
            0.1, 0.9, data['cell_type'].iloc[0].split('-')[-1], 
            fontweight='bold',
            transform=ax.transAxes, 
            color=CMAP_CELLS[data['cell_type'].iloc[0]], 
            size=fontsize
        )
        plot_tetrahedron(
            ax, r, Q, est, psth, radius=radius, 
            axins=axin, rescale=True, label_size='xx-small', 
            point_size=point_size, 
            point_offset=point_offset, 
            Q_min=0, 
            Q_max=100, 
        )
        
        
def plot_model_scores(
    ax, 
    data_scores, 
    cell_types, 
    model_colors, 
    models, 
    legend_kwargs={}
):
    data_scores = data_scores[
        data_scores['cell_type'].isin(cell_types)
    ].copy()
        
    ax = sns.barplot(
        data=data_scores, 
        order=cell_types, 
        x='cell_type', 
        y='scores', 
        palette=model_colors, 
        hue='names', 
        alpha=1.0, 
        ci=None, 
        ax=ax, 
        linewidth=1, 
        hue_order=models, 
        estimator=np.median,
        legend=False
    )
    for patch in ax.patches:
        clr = patch.get_facecolor()
        patch.set_edgecolor(clr)
        patch.set_facecolor('none')
        width = patch.get_width()
        patch.set_width(width * 3/4)
    handles, labels = ax.get_legend_handles_labels()

    ax = sns.stripplot(
        data=data_scores, 
        order=cell_types,
        x='cell_type',
        hue='names', 
        y='scores', 
        palette=model_colors,
        hue_order=models, 
        dodge=True,
        size=2, edgecolor='white', 
        linewidth=0.25,
        ax=ax, 
        legend=False
    )
        
    ax.set_ylim([0, 1])
    xlabels = ax.get_xticklabels()
    ax.set_xticklabels(xlabels, rotation = 90)
    for ticklabel in ax.get_xticklabels():
        if not ticklabel:
            continue
        tickcolor = CMAP_CELLS[ticklabel.get_text()]
        ticklabel.set_color(tickcolor)
    ax.set_ylabel('cross-validated $R^2$')
    ax.set_xlabel(None)
    sns.despine(ax=ax)
    ax.legend(handles, labels, frame=False, **legend_kwargs)
    # ax.legend([], [], frame=False)
    
    # r = matplotlib.patches.Patch(color='none')
    # ax.legend(
    #     [r]*len(labels), labels, 
    #     frame=False, 
    #     ncols=3, 
    #     labelcolor=list(model_colors.values()), 
    #     loc='ur'
    # )
    return ax


def plot_kappas(
    ax, tau, cell_types, 
    yticks=[0, 5, 10], 
    arange=(0, 10), bins=20, 
    name='tau', label='kappa'
):
    plot_utils.vertical_histogram(
        data=tau.reset_index(), 
        x='cell_type', 
        y=name, 
        mean=f'{name}_mean',  
        mean_separate=True, 
        mean_same_color=False, 
        order=cell_types, 
        ax=ax, 
        arange=arange,
        bins=bins, 
        hue='cell_type',
        # color='gray', 
        palette_dict=CMAP_CELLS,
        alpha=1.0,
        linestyle='',
        mean_kwargs={'color': 'black', 'alpha': 1.0, 'linewidth': 1}, 
        mean_dist_kwargs={'linestyle': '-', 'linewidth': 2, 'color': 'lightgray'}
    )
    ax.set_ylabel(f'$\\{label}^*$')
    ax.set_xlabel(None)
    ax.set_yticks(yticks)
    ax.set_xticklabels(ax.get_xticklabels(), rotation = 90)
    for ticklabel in ax.get_xticklabels():
        tickcolor = CMAP_CELLS[ticklabel.get_text()]
        ticklabel.set_color(tickcolor)
    sns.despine(ax=ax)
    
    

def distribution_plot(data, cols, bins, label_format, ylim=(0, 40)):
    fig, axs = pplt.subplots(
        nrows=len(cols), 
        ncols=len(cols), 
        journal='nat2', 
        sharey=False,
        sharex=False,
        equal=True
    )
    binrange = (
        np.floor(data[cols].to_numpy().min()), 
        np.ceil(data[cols].to_numpy().max()), 
    )


    for (idx1, c1), (idx2, c2) in product(enumerate(cols), repeat=2):
        if idx1 == idx2:
            ax = axs[idx1, idx2]
            sns.histplot(
                data=data, 
                x=c1, 
                color='gray',
                element='step', 
                alpha=0.5,
                binrange=binrange, 
                bins=bins,
                ax=ax, 
                stat='percent'
            )
            ax.set_ylabel('Stimulus distribution (%)')
            ax.set_ylim(*ylim)
            ax.set_xlim(*binrange)
            sns.despine(ax=ax)
        else:
            ax = axs[idx1, idx2]
            
            if idx1 > idx2:      
                sns.scatterplot(
                    data=data, 
                    x=c1, y=c2, 
                    ax=ax, 
                    c='gray', 
                    s=5,
                )
                
            else:
                sns.histplot(
                    data=data, 
                    x=c1, y=c2, 
                    ax=ax, 
                    color='gray', 
                    alpha=0.5,
                    binrange=binrange, 
                    bins=bins,
                )
            ax.set_ylim(*binrange)
            ax.set_xlim(*binrange)
            ax.set_ylabel(label_format.format(col=c1))
        ax.set_xlabel(label_format.format(col=c2))
        ax.set_xlim(*binrange)    
        sns.despine(ax=ax)
    fig.format(
        abc=True, includepanels=True
    )
    return fig, axs

