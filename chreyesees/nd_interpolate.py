
import numpy as np

from scipy.interpolate import LinearNDInterpolator
from tqdm import trange
from chreyesees.rbf.rbfinterp import RbfModel
import warnings


def nan_rbf_interpolator(X, Y, sample_weight=None, copy=True, error='ignore', **kwargs):
    if copy:
        Y = Y.copy()

    if sample_weight is None:
        sample_weight = np.ones_like(Y)
    
    for idx, y in enumerate(Y.T):
        isfinite = np.isfinite(y)
        if not np.any(isfinite):
            message = f"All elements for column {idx} in Y are NaNs."
            if error == 'ignore':
                pass
            elif error == 'warn':
                warnings.warn(message, RuntimeWarning)
            else:
                raise ValueError(message)
        else:
            model = RbfModel(**kwargs)
            model.fit(X[isfinite], y[isfinite], sample_weight=sample_weight[isfinite, idx])
            Y[~isfinite, idx] = model.predict(X[~isfinite])
        
    return Y
        


class CompleteMeRBFInterpolator:
    
    def __init__(self, X, Y, snrs, **kwargs):
        X = np.asarray(X)
        Y = np.asarray(Y)
        snrs = np.asarray(snrs)
        
        interps = []
        for snr, y in zip(snrs, Y.T):
            isfinite = np.isfinite(y)
            interp = RbfModel(
                smoothing=1/snr,
                **kwargs
            ).fit(X[isfinite], y[isfinite])
            interps.append(interp)
        
        self.interps = interps
            
    def __call__(self, Xnew):
        Xnew = np.asarray(Xnew)
        return np.stack([interp.predict(Xnew) for interp in self.interps], axis=-1)


class MeanNdInterpolator:
    """Interpolator
    """
    
    def __init__(
        self, X, y, 
        fraction=0.3, 
        average=False,
        average_fraction=0.5,
        minimum_samples=10,  # only relevant, if average is True
        n_iters=10000, 
        seed=None, 
        interp_class=LinearNDInterpolator, 
        unique=False, 
        stat=np.nanmedian,
        bootstrap=False,
        **kwargs
    ):
        rng = np.random.default_rng(seed)
        
        self.stat = stat
        self.fraction = fraction
        self.n_iters = n_iters
        self.interp_class = interp_class
        
        X = np.asarray(X)
        y = np.asarray(y)
        if unique:
            Xu = np.unique(X, axis=0)
        
        average = y.ndim > 1 and average
        self.y_shape = y.shape[1:]
        
        interps = []
        for _ in trange(n_iters):
            # only choose from the unique samples
            if unique:
                if bootstrap:
                    idcs = rng.integers(Xu.shape[0], size=int(Xu.shape[0] * fraction))
                else:
                    idcs = rng.choice(Xu.shape[0], size=int(Xu.shape[0] * fraction))
                
                idcs_ = []
                for xu in Xu[idcs]:
                    xbool = np.all(X == xu, -1)
                    idcs_.append(rng.choice(np.where(xbool)[0]))                  
                X_ = X[idcs_]
                y_ = y[idcs_]
               
            # choose from samples ignoring uniqueness 
            else:
                if bootstrap:
                    idcs = rng.integers(X.shape[0], size=int(X.shape[0] * fraction))
                else:
                    idcs = rng.choice(X.shape[0], size=int(X.shape[0] * fraction))
                X_ = X[idcs]
                y_ = y[idcs]
                
            if average:
                if average_fraction < 1.0:
                    jdcs = rng.choice(y_.shape[1], size=int(y_.shape[1] * average_fraction))
                    y_ = np.nanmean(y_[:, jdcs], axis=-1)
                else:
                    y_ = np.nanmean(y_, axis=-1)
                
                isfinite = np.isfinite(y_)
                if np.sum(isfinite) < minimum_samples:
                    continue
                
                y_ = y_[isfinite]
                X_ = X_[isfinite]
                
            interp = interp_class(X_, y_, **kwargs)
            interps.append(interp)
            
        self.interps = interps
        self.true_iters = len(self.interps)
        
        if self.true_iters != self.n_iters:
            print(f"The true number of iterations is smaller due to sample size constraints: {self.true_iters}")
                
    def __call__(self, Xnew, return_samples=False):
        Xnew = np.asarray(Xnew)
        ynew = np.zeros((self.true_iters, Xnew.shape[0]) + self.y_shape)
        for idx in trange(self.true_iters):
            interp = self.interps[idx]
            ynew[idx] = interp(Xnew)
        if return_samples:
            return ynew
        return self.stat(ynew, 0)
        