import numpy as np
from itertools import product
from dreye.api import barycentric, spherical
import seaborn as sns
from scipy.spatial import ConvexHull
from dreye.api.plotting.simplex_plot import plot_simplex

from chreyesees import TINTERP_S
from chreyesees import plot_scalebar
from mpl_toolkits.mplot3d import axes3d  # necessary for inset
from matplotlib.transforms import Bbox
import matplotlib.patches as patches
from sklearn.preprocessing import normalize
from chreyesees.palettes import CMAP_OPSINS

from chreyesees.utils import bootstrapped_stat


CMAP = 'phase'
CMAP_DOMAIN = np.arange(300, 540, 1)
CMAP_VMIN = 260
CMAP_VMAX = 580
Q_RESPONSE_NORM = 98

# TODO cleanup

def transform_and_rescale_pts(B, optimal, transform, rescale):
    # transforming capture values
    if transform is not None:
        optimal = transform(optimal)
        if B is not None:
            B = transform(B)
            
    if rescale:
        optimal = normalize(optimal, norm='l1', axis=1)
        omax = optimal.max(0)
        
        if rescale == 'max':
            omin = 0
        else:
            omin = optimal.min(0)
        
        optimal = (optimal - omin) / (omax - omin)
        if B is not None:
            B = normalize(B, norm='l1', axis=1)
            B = (B - omin) / (omax - omin)
    return B, optimal

# messy plotting function
def plot_tetrahedron(
    ax, r_gamut, Q_gamut, est, psth_mean,
    point_size=5, point_offset=5, cmap='coolwarm', 
    q=Q_RESPONSE_NORM, Q_min=2, Q_max=98, 
    radius=0.075, do_tetra_inset=False, 
    rescale=True, transform=None, 
    scale_size=False,
    view=(15, -30), axins=None, label_size='x-small', 
    do_psths=True, 
):
    # sanity checks
    assert Q_gamut.shape[1] == 4
    assert est.filters.shape[0] == 4

    # reformat responses
    y = r_gamut
    ymax = np.percentile(np.abs(y), q=q)
    y = y / ymax
    points = Q_gamut
    
    kws = {}
    if scale_size:
        kws['s'] = (np.abs(y)*point_size+point_offset) 
    else:
        kws['s'] = point_size
    
    # plot points in main simplex plot
    ax = est.simplex_plot(
        points, add_hull=False, add_center=False,
        cmap_B=cmap, cmap=CMAP,
        domain=CMAP_DOMAIN,
        c=y, vmin=-1, vmax=1,
        gradient_line_kws={'vmin': CMAP_VMIN, 'vmax': CMAP_VMAX},
        ax=ax, 
        label_size='small', transform=transform, rescale=rescale, 
        label_kws=[
            {'ha':'right', 'va':'center', 'color': CMAP_OPSINS['rh3']},
            {'ha':'center', 'va':'top', 'color': CMAP_OPSINS['rh4']},
            {'ha':'left', 'va':'center', 'color': CMAP_OPSINS['rh5']},
            {'color': CMAP_OPSINS['rh6']},
        ], 
        **kws, 
    )
    
    # processing for inset plots
    
    # three d loc
    if do_tetra_inset:
        xfrac, yfrac = 0.4, 0.4
        inset_loc = (-0.05, 0.5)
        threed_axins = add_inset_axes((*inset_loc, xfrac, yfrac), projection='3d', ax_target=ax, units="norm2ax", frame_on=False, facecolor='none')
        est.simplex_plot(
            add_hull=False, add_center=False,
            cmap=CMAP,
            domain=CMAP_DOMAIN,
            vmin=-1, vmax=1,
            gradient_line_kws={'vmin': CMAP_VMIN, 'vmax': CMAP_VMAX},
            zorder=-1,
            ax=threed_axins, 
            label_size=label_size, 
            transform=transform, rescale=rescale
        )
    else:
        threed_axins = None
        
    if do_psths:
        # this function exists in simplex_plot of the estimator here just
        # to use it for the radius stuff
        # dirac delta functions for perfect excitation
        signals = np.eye(est.filters.shape[-1])
        optimal = est.relative_capture(signals)
        points, optimal = transform_and_rescale_pts(points, optimal, transform, rescale)
        # shared things
        bary = barycentric.barycentric_dim_reduction(points)
        
        # fracs for all
        xfrac, yfrac = 0.25, 0.25
        white_loc = (0.65, 0.6)
        lecenter = barycentric.barycentric_dim_reduction(np.ones(points.shape[1])[None])
        # baseline_bool = (TINTERP >= BASELINE_INTERVAL[0]) & (TINTERP <= BASELINE_INTERVAL[1])
        
        # min max
        min_idx = np.argmin(np.abs(y - np.percentile(y, q=Q_min)))
        max_idx = np.argmin(np.abs(y - np.percentile(y, q=Q_max)))
        
        yvalues = np.linspace(-1, 1, 101)
        ycolors = sns.color_palette(cmap, yvalues.size)
        
        min_loc = np.argmax(points[min_idx])
        max_loc = np.argmax(points[max_idx])
        
        if min_loc > max_loc:
            min_loc = (0.65, 0)
            max_loc = (0, 0)
        else:
            min_loc = (0, 0)
            max_loc = (0.65, 0)
            
        if not do_tetra_inset:
            white_loc = (0.325, -0.2)
            min_loc = (0, -0.2)
            max_loc = (0.65, -0.2)
            
        
        if axins is None: 
            axin_white = ax.inset_axes((*white_loc, xfrac, yfrac))
            axin_min = ax.inset_axes((*min_loc, xfrac, yfrac))
            axin_max = ax.inset_axes((*max_loc, xfrac, yfrac))
        else:
            axin_min, axin_white, axin_max = axins
        
        axins = [axin_white, axin_min, axin_max]
        positions = [
            lecenter, bary[min_idx], bary[max_idx]
        ]
        titles = ["\"fly white\"", 'min', 'max']
        ymin, ymax = 0, 0
        for axin, title, position in zip(axins, titles, positions):
            
            _plot_hull(
                threed_axins, 
                axin, est, position,
                bary, radius, psth_mean, 
                y, ycolors, yvalues, title,
            )
            
            ymin_, ymax_ = axin.get_ylim()
            ymin = np.min([ymin_, ymin])
            ymax = np.max([ymax_, ymax])
            
        yrange = ymax - ymin
        ymin = ymin - yrange * 0.1
        ymax = ymax + yrange * 0.1
            
        for idx, axin in enumerate(axins):
                
            axin.set_ylim(ymin, ymax)
            axin.axvline(0, linestyle='--', color='gray', alpha=0.4)
            axin.axhline(0, linestyle='--', color='gray', alpha=0.4)
            axin.axvline(0.5, linestyle='--', color='gray', alpha=0.4)
            
            if (idx != 1) and not do_tetra_inset:
                sns.despine(ax=axin, bottom=True, left=True)
                axin.set_xticks([])
                axin.set_yticks([])
                continue
            
            plot_scalebar.scalebar(
                ax=axin, 
                xsize=1,
                ysize=1,
                xunits='s',
                yunits='dF/F',
                fontsize='small',
            )
            sns.despine(ax=axin)
            
    ax.view_init(*view)
    if threed_axins is not None:
        threed_axins.view_init(*view)
    return ax


def add_inset_axes(rect, units="ax", ax_target=None, fig=None, projection=None, **kw):
    """
    Wrapper around `fig.add_axes` to achieve `ax.inset_axes` functionality
    that works also for insetting 3D plot on 2D ax/figures
    """
    assert ax_target is not None or fig is not None, "`fig` or `ax_target` must be provided!"
    _units = {"ax", "norm2ax", "norm2fig"}
    assert {units} <= _units, "`rect_units` not in {}".format(repr(_units))

    if ax_target is not None:
        # Inspired from:
        # https://stackoverflow.com/questions/14568545/convert-matplotlib-data-units-to-normalized-units
        bb_data = Bbox.from_bounds(*rect)
        trans = ax_target.transData if units == "ax" else ax_target.transAxes
        disp_coords = trans.transform(bb_data)
        fig = ax_target.get_figure()
        fig_coord = fig.transFigure.inverted().transform(disp_coords)
    elif fig is not None:
        if ax_target is not None and units != "norm2fig":
            bb_data = Bbox.from_bounds(*rect)
            trans = ax_target.transData if units == "ax" else ax_target.transAxes
            disp_coords = trans.transform(bb_data)
        else:
            fig_coord = Bbox.from_bounds(*rect)

    axin = fig.add_axes(
        Bbox(fig_coord),
        projection=projection, **kw)

    return axin


def _plot_hull(
    threed_axins, 
    axin,
    est, 
    position, bary, radius, 
    psth_mean, 
    y, ycolors, yvalues, title,
    q=(16, 84), # "standard deviation"
    ci=95,
    edgecolor=None, 
    zorder=10,
    alpha_area=0.5
):
    dists = np.linalg.norm((bary - position), axis=-1)
    angles = np.array(list(product(np.linspace(0, np.pi, 11), np.linspace(0, 2*np.pi, 11))))

    sph_coords = np.hstack([np.ones((len(angles), 1))*radius, angles])
    ball_coords = spherical.spherical_to_cartesian(sph_coords) + position

    idcs = np.where(dists < radius)[0]
    hull = ConvexHull(ball_coords)
    
    # redoing facecolor
    facecolor = ycolors[np.argmin(np.abs(yvalues - np.nanmedian(y[idcs])))]
    
    rect = patches.Rectangle(
        (0,0), 1, 1, linewidth=4, edgecolor=facecolor, 
        facecolor='none', transform=axin.transAxes
    )
    axin.add_patch(rect)
    axin.set_title(title, fontdict={'fontsize': 'x-small'})
    
    if threed_axins is not None:
        plot_simplex(
            ax=threed_axins, n=est.filters.shape[0], 
            lines=False, hull=hull, hull_kws={
                'facecolor': facecolor, 
                'edgecolor': edgecolor, 
                'zorder': zorder,
                'alpha': alpha_area
            }
        )
    if psth_mean.ndim == 2:
        # stim, time
        psth_mean_ = np.nanmedian(psth_mean[idcs], 0)
        psth_low_, psth_high_ = np.nanpercentile(psth_mean[idcs], q=q, axis=0)
        n = len(idcs)
    elif psth_mean.ndim == 3:
        # stim, roi, time
        # average roi across its stims
        # roi, time
        psth_mean = np.nanmean(psth_mean[idcs], axis=0)
        # finite rois
        isfinite = np.isfinite(psth_mean[..., 0])
        n = np.sum(isfinite)
        psth_mean = psth_mean[isfinite]
        psth_mean_ = np.nanmean(psth_mean, axis=0)
        # time, roi
        psth_low_, psth_high_ = bootstrapped_stat(psth_mean.T, stat=np.nanmean, ci=ci)
    else:
        raise ValueError(f"psth_mean dimension must be 2 or 3, but is {psth_mean.ndim}")
    
    axin.fill_between(
        TINTERP_S, psth_low_, psth_high_, 
        color='gray', alpha=0.5
    )

    axin.plot(
        TINTERP_S, 
        psth_mean_, 
        color='black', 
    )
    
    axin.text(
        1, 1, f"n={n}", 
        transform=axin.transAxes,
        ha='right', va='top', 
        fontsize='x-small'
    )
    


