from numbers import Number
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import puffbird as pb
import seaborn as sns
import proplot as pplt
from scipy.stats import norm
from chreyesees import utils
from chreyesees import WLS
import dreye
from scipy.spatial import convex_hull_plot_2d, ConvexHull

from sklearn.preprocessing import normalize, minmax_scale
from chreyesees.plot_scalebar import scalebar
from chreyesees.plot_tetrahedron import CMAP, CMAP_DOMAIN, CMAP_VMAX, CMAP_VMIN
from chreyesees.tick_curves import tick_curves
from chreyesees.utils import twod_transform, rayleigh_vec


def single_wls_line(
    est, ax=None
):
    if ax is None:
        ax = plt.gca()
        
    # spectra = np.eye(
    #     est.filters.shape[1]
    # )[(WLS >= CMAP_DOMAIN.min()) & (WLS <= CMAP_DOMAIN.max())]
    
    wls = np.arange(320, 540+1, 10)
    spectra = np.eye(
        est.filters.shape[1]
    )[np.isin(WLS, wls)]
    single_qs = est.relative_capture(spectra)
    single_2d = twod_transform(single_qs)
    # ax.plot(
    #     single_2d[:, 0], single_2d[:, 1], c='black'
    # )
    tick_curves(
        single_2d[:, 0], single_2d[:, 1],
        labels = wls,
        ax=ax, 
        skip_ticks=2, 
        lw=1, 
        tick_len=-0.2, text_offset=-0.8,
    )
    return ax
    
def twod_labels(ax=None):
    if ax is None:
        ax = plt.gca()
        
    ax.set_xlabel('Rh3+Rh4-Rh5-Rh6')
    ax.set_ylabel('-Rh3+Rh4+Rh5-Rh6')
    sns.despine(ax=ax, trim=True, offset=5)
    return ax

def two_scalebar(ax=None):
    if ax is None:
        ax = plt.gca()
        
    return scalebar(
        ax=ax, xsize=3, ysize=3, 
        xunits='Rh3+Rh4\n-Rh5-Rh6', 
        yunits='-Rh3+Rh4\n+Rh5-Rh6',
        xformat=True, yformat=True,
        xleft=-0.15, ybottom=-0.05,
    )

def plot_hull(est: dreye.ReceptorEstimator, ax=None):
    if ax is None:
        ax = plt.gca()
        
    q = est.system_relative_capture(
        np.eye(est.sources.shape[0])
    )
    q_proj = twod_transform(q)
    hull = ConvexHull(q_proj)
    
    from matplotlib.collections import LineCollection

    if hull.points.shape[1] != 2:
        raise ValueError("Convex hull is not 2-D")

    # ax.plot(hull.points[:, 0], hull.points[:, 1], 'o')
    line_segments = [
        hull.points[simplex] 
        for simplex in hull.simplices
    ]
    ax.add_collection(
        LineCollection(
        line_segments,
        colors='gray',
        alpha=0.5, 
        linestyle='--', ))
    
    _adjust_bounds(ax, hull.points)
    return ax


def _adjust_bounds(ax, points):
    margin = 0.1 * points.ptp(axis=0)
    xy_min = points.min(axis=0) - margin
    xy_max = points.max(axis=0) + margin
    ax.set_xlim(xy_min[0], xy_max[0])
    ax.set_ylim(xy_min[1], xy_max[1])


def plot_projected(
    X, y, ax=None,
    norm_q=95,
    add_arrow=True, 
    as_contour=False, 
    cmap="coolwarm", 
    counts=None,
    **kwargs
):
    if ax is None:
        ax = plt.gca()
        
    Xproj = twod_transform(X)
    y = np.array(y)
    
    y = y / np.percentile(np.abs(y), q=norm_q)
    
    if as_contour:
        # import matplotlib.tri as tri
        # triang = tri.Triangulation(Xproj[:, 0], Xproj[:, 1])
        # refiner = tri.UniformTriRefiner(triang)
        # tri_refi, z_test_refi = refiner.refine_field(y, subdiv=3)
        # x1, x2 = tri_refi.edges.T
        # print(tri_refi.neighbors.shape)
        # ax.tricontour(
        #      x1, x2,
        #      z_test_refi, 
        #     cmap=cmap, 
        #     vmin=-1, vmax=1, 
        #     **kwargs
        # )
        ax.tricontour(
            Xproj[:, 0], Xproj[:, 1], 
            y,
            cmap=cmap, 
            vmin=-1, vmax=1, 
            **kwargs
        )
    else:
        ax.scatter(
            Xproj[:, 0], Xproj[:, 1], 
            c=y,
            cmap=cmap, 
            vmin=-1, vmax=1, 
            **kwargs
        )
    
    if add_arrow:
        # ynorm = (y - np.min(y))/(np.max(y) - np.min(y))
        # tops = np.percentile(y, q=90)
        # ybool = y > tops
        # yvec = np.average(
        #     Xproj[ybool], 
        #     axis=0, 
        #     weights=ynorm[ybool]
        # )
        # Xproj = twod_transform(X, divmean=False)
        if counts is None:
            counts = np.ones(y.shape)

        yvec = rayleigh_vec(counts, Xproj, y) * 3
                
        # yvec = np.sum(Xproj * y[:, None], axis=0)/np.sum(np.abs(y))
        ax.arrow(
            0, 0, yvec[0], yvec[1], color='black'
        )
    
    return ax