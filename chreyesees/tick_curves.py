import numpy as np
from matplotlib.collections import LineCollection
import matplotlib.pyplot as plt


def tick_curves(
    x0, y0, labels, idx=None,
    ax=None, lw=3,
    color='black', tick_len=0.2,
    text_offset=0.5, skip=1,
    skip_ticks=1, 
    fontsize="xx-small",
    **text_kwargs
):
    """
    create ticked lines.
    Use: Single wavelengths in 2D color space
    """

    if ax is None:
        ax = plt.gca()
    if idx is None:
        idx = np.arange(1, len(labels)-1)[::skip_ticks]
        
    ax.plot(x0, y0, lw=lw, color=color)

    x, y = x0[idx], y0[idx]
    xp, yp = x0[idx-1], y0[idx-1]
    xn, yn = x0[idx+1], y0[idx+1]

    labels = labels[idx]
    angle = np.arctan2(yn-yp, xn-xp) + np.pi / 2
    x1, y1 = x + tick_len*np.cos(angle), y + tick_len*np.sin(angle)
    x2, y2 = x + text_offset*np.cos(angle), y + text_offset*np.sin(angle)

    tick_lines = LineCollection(
        np.c_[x, y, x1, y1].reshape(-1, 2, 2), color=color, lw=lw)
    ax.add_collection(tick_lines)

    for i in range(len(idx)):
        if i % skip != 0:
            continue

        ax.text(x2[i], y2[i], str(labels[i]), va="center", ha="center", fontsize=fontsize, **text_kwargs)
        
    return ax