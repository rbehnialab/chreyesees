"""
Tables for processing individual recordings
"""

"""
Main tables for loading individual recordings and processing responses
"""


from glob import glob
import os
import warnings

import numpy as np
import pandas as pd
import datajoint as dj
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.stats import spearmanr
from sklearn.decomposition import TruncatedSVD
from sklearn.linear_model import LinearRegression

from chreyesees.utils import (
    calculate_capture, calculate_dynamic_range, 
    calculate_sparsity, 
    load_bg_ints, signif, 
)
from chreyesees.db import schema

from . import (
    BG_QS, BG_QS_VAR, DATA_FOLDERPATH, BG_LEDS, LEDS, 
    MASTER_FOLDER, NULL_BEFORE_OFFSET, 
    NULL_S_AFTER, NULL_S_BEFORE, NULL_T_AFTER, 
    NULL_T_BEFORE, OPSINS, QS, QS_VAR, ROC_FALSE_POS, ROC_TRUE_POS, 
    SMOOTHING_WINDOW, TBOOL_AMP, TBOOL_BASELINE, TBOOL_OFF, TINTERP, 
    CI, WLS
)


@schema
class CellName(dj.Manual):
    definition = """
    cell_type : varchar(63)
    """

@schema
class CellTypes(dj.Manual):
    definition = """
    -> CellName
    genotype_id : int
    ---
    """
    
cell_types = [
    ('Tm5b', 1191), 
    ('Tm5b', 1210), 
    ('pR7', 517), 
    ('yR7', 518), 
    ('pR8', 519), 
    ('yR8', 529), 
    ('Tm5a', 1428), 
    ('Tm5a', 1254), 
    ('Tm5c', 952), 
    ('Tm20', 1929), 
    ('pDm8', 908), 
    ('pDm8', 830), 
    ('yDm8', 777), 
    ('Tm5b-GCaMP-TNT', 2293), 
    ('Tm5a-TnT', 2368),
    ('Tm20-Tnt', 2511)
]
cell_names = [(cell_type[0],) for cell_type in cell_types]

CellName.insert(cell_names, skip_duplicates=True)    
CellTypes.insert(cell_types, skip_duplicates=True)


@schema
class OpsinSensitivity(dj.Manual):
    definition = """
    opsin_name : varchar(127)
    ---
    sense : longblob
    sense_var : longblob
    """
    
    def populate(self):
        sense_folder = os.path.join(MASTER_FOLDER, 'data', 'sensitivities')
        sense_files = glob(os.path.join(sense_folder, '*.csv'))
        for sense_file in sense_files:
            
            sense_name = os.path.basename(sense_file).replace('.csv', '')
            
            S = pd.read_csv(sense_file).set_index('wls')
            wls = S.index.to_numpy()
            
            # every second column is vars
            # some data wrangling
            has_vars = (S.shape[1] % 2 == 0)
            every_second = True
            if has_vars:
                has_vars = np.all(['var' in col for col in S.columns[::2]])
                if not has_vars:
                    # or last part
                    has_vars = np.all(
                        ['var' in col for col in S.columns[len(S.columns)//2:]]
                    )
                    every_second = False
                
            if S.shape[1] == 1:
                S = S.to_numpy().squeeze()
                self.insert1_opsin(
                    sense_name, wls, S, 
                    skip_duplicates=True
                )
            elif S.shape[1] == 2 and has_vars:
                S_var = S.iloc[:, 1].to_numpy().squeeze()
                S = S.iloc[:, 0].to_numpy().squeeze()
                self.insert1_opsin(
                    sense_name, wls, S, S_var, 
                    skip_duplicates=True
                )
            elif has_vars:
                if every_second:
                    for s, s_var, name in zip(
                        S.to_numpy().T[::2], 
                        S.to_numpy().T[1::2], 
                        S.columns[::2]
                    ):
                        self.insert1_opsin(
                            f"{sense_name}_{name}", wls, s, s_var, skip_duplicates=True
                        )
                else:
                    for s, s_var, name in zip(
                        S.to_numpy().T[:len(S.columns)//2], 
                        S.to_numpy().T[len(S.columns)//2:], 
                        S.columns[:len(S.columns)//2]
                    ):
                        self.insert1_opsin(
                            f"{sense_name}_{name}", wls, s, s_var, skip_duplicates=True
                        ) 
            else:
                for s, name in zip(S.to_numpy().T, S.columns):
                    self.insert1_opsin(
                        f"{sense_name}_{name}", wls, s, skip_duplicates=True
                    )    
    
    def insert1_opsin(self, name, wls : np.ndarray, S, S_var=None, **kwargs):
        if S_var is None:
            # poisson capture noise model
            S_var = S
        
        if (wls.shape == WLS.shape) and np.all(wls == WLS):
            pass
        else:
            S = interp1d(wls, S)(WLS)
            S_var = interp1d(wls, S_var)(WLS)
        
        # cannot be negative
        S_var[S_var < 0] = 0
        S[S < 0] = 0
        
        key = {
            'sense': S, 
            'sense_var': S_var, 
            'opsin_name': name, 
        }
        self.insert1(key, **kwargs)
    
 
@schema   
class SensitivitySet(dj.Manual):
    definition = """
    set_name : varchar(127)
    ---
    """ + "\n".join([
        f"-> OpsinSensitivity.proj({rh}='opsin_name')"
        for rh in OPSINS
    ])
    
    def populate(self):
        self.insert1_sense(
            'standard', 
            ['rh1_standard', 'rh3_morning', 'rh4_morning', 'rh5_morning', 'rh6_morning'], 
            skip_duplicates=True
        )
        
        self.insert1_sense(
            'govardoskii', 
            ['rh1_standard', 'govardoskii_morning_rh3', 'govardoskii_morning_rh4', 'govardoskii_morning_rh5', 'govardoskii_morning_rh6'], 
            skip_duplicates=True
        )
        
        self.insert1_sense(
            'sharkey', 
            ['sharkey2020_rh1', 'sharkey2020_rh3', 'sharkey2020_rh4', 'sharkey2020_rh5', 'sharkey2020_rh6'], 
            skip_duplicates=True
        )
        
        self.insert1_sense(
            'refit', 
            ['rh1_standard', 'refit_2022_rh3', 'refit_2022_rh4', 'refit_2022_rh5', 'refit_2022_rh6'], 
            skip_duplicates=True
        )
        
        self.insert1_sense(
            'refixed', 
            ['rh1_standard', 'refixed_2022_rh3', 'refixed_2022_rh4', 'refixed_2022_rh5', 'refixed_2022_rh6'], 
            skip_duplicates=True
        )
        
        self.insert1_sense(
            'separate', 
            ['rh1_standard', 'separate_rh3', 'separate_rh4', 'separate_rh5', 'separate_rh6'], 
            skip_duplicates=True
        )
        
        self.insert1_sense(
            'tested', 
            ['rh1_standard', 'completeness_rh3', 'completeness_rh4', 'completeness_rh5', 'completeness_rh6'], 
            skip_duplicates=True
        )
        
        self.insert1_sense(
            'salcedo', 
            ['salcedo2003_rh1', 'salcedo2003_rh3', 'salcedo2003_rh4', 'salcedo2003_rh5', 'salcedo2003_rh6'], 
            skip_duplicates=True
        )
    
    def insert1_sense(self, set_name, opsin_names, **kwargs):
        key = {'set_name': set_name}
        assert len(opsin_names) == len(OPSINS), "length mismatch."
        for rh, opsin_name in zip(OPSINS, opsin_names):
            key[rh] = opsin_name
        self.insert1(key, **kwargs)
    
    def fetch1_sense(self, set_name, return_var=False):
        opsin_names = (self & {'set_name': set_name}).fetch1(*OPSINS)
        
        S = pd.DataFrame(index=WLS)
        S_var = pd.DataFrame(index=WLS)
        
        for rh, opsin_name in zip(OPSINS, opsin_names):
            key = {'opsin_name': opsin_name}
            S[rh], S_var[rh] = (OpsinSensitivity & key).fetch1('sense', 'sense_var')
            
        if return_var:
            return S, S_var
        return S


@schema
class StimulusSet(dj.Manual):
    definition = """
    stimulus_set : varchar(63)
    ---
    """
    
StimulusSet.insert([
    {'stimulus_set': 'gamut'}, 
    {'stimulus_set': 'exploration'}, 
    {'stimulus_set': 'ratio'}
], skip_duplicates=True)


@schema
class RecordingIds(dj.Manual):
    definition = """
    -> StimulusSet
    recording_id : int
    ---
    """
    
    def populate(self):
        for key in StimulusSet:
            name = key['stimulus_set']
            
            folder = os.path.join(DATA_FOLDERPATH, name)
            
            recs = [
                {
                    'stimulus_set' : name,
                    'recording_id' : int(os.path.basename(rec).replace('rec', ''))
                } 
                for rec in glob(os.path.join(folder, 'rec*'))
            ]
            self.insert(recs, skip_duplicates=True)

@schema
class Background(dj.Manual):
    definition = """
    bg_name : varchar(63)
    bg_int : int
    --- 
    # background led intensities in nE
    """ + "\n".join(
        f"{led} : int"
        for led in BG_LEDS
    )
    

@schema
class Recording(dj.Imported):
    definition = """
    -> RecordingIds
    ---
    genotype_id : int
    subject_id : int
    sex : enum('F', 'M', 'U')
    age = null : float
    rearing_method = null : varchar(127)
    chr1 = null : varchar(511) 
    chr2 = null : varchar(511) 
    chr3 = null : varchar(511)
    recording_time : timestamp
    recording_solution = null : varchar(63)
    neuron_section = null : varchar(255)
    brain_area = null : varchar(255)
    rate : double  # in fps
    -> Background
    total_start : int  # in ms
    total_end : int  # in ms
    times : longblob  # array of timestamps in ms
    """
    
    class Roi(dj.Part):
        definition = """
        -> master
        cell_id : int
        ---
        dfof : longblob  # array of dfof signal (same shape as times)
        raw : longblob  # array of raw signal (same shape as times)
        com_x : double  # location in frame
        com_y : double  # location in frame
        size : int  # size of ROI in pixels
        """
    
    class Events(dj.Part):
        definition = """
        -> master
        delay : int
        ---
        dur : int
        pause : int
        # absolute LED intensities in nE
        """ + "\n".join(
            f"{led} : int"
            for led in LEDS
        )
    
    # stimulus-set-specific part tables
    
    class GamutInfo(dj.Part):
        definition = """
         -> master
        delay : int
        ---
        color_space_id : int
        metamer_id : int
        """
    
    class ExplorationInfo(dj.Part):
        definition = """
        -> master
        delay : int
        ---
        led_combos : varchar(63)
        diff : int
        led_number : int
        is_off : tinyint
        """
        
    class RatioInfo(dj.Part):
        definition = """
        -> master
        delay : int
        ---
        led_combos : varchar(63)
        led_combo1 : varchar(15)
        led_combo2 : varchar(15)
        ratio : double
        rel_abs_sum : int
        """
        
    def make(self, key):
        # print(f"process recording: {key}")
        name = key['stimulus_set']
        recfolder = os.path.join(
            DATA_FOLDERPATH, 
           name, 
            f"rec{key['recording_id']}"
        )
        rec_metadata = pd.read_json(
            os.path.join(recfolder, 'rec_metadata.json'), typ='series', 
            date_unit='s'
        )
        # skip non-inserted genotypes
        if not len(CellTypes & {'genotype_id': rec_metadata['genotype_id']}):
            return
                
        dfof = np.load(os.path.join(recfolder, 'dfof.npy'))
        raw = np.load(os.path.join(recfolder, 'raw.npy'))
        times = np.load(os.path.join(recfolder, 'times.npy'))
        
        events = pd.read_json(os.path.join(recfolder, 'events.json'))
        roi_metadata = pd.read_json(
            os.path.join(recfolder, 'roi_metadata.json'))
        stimulus_metadata = pd.read_json(
            os.path.join(recfolder, 'stimulus_metadata.json'), typ='series'
        )
        
        reckey = rec_metadata.to_dict()        
        assert reckey['recording_id'] == key['recording_id'], "Recording id mismatch in metadata."
        
        bg_dict = dict(zip(BG_LEDS, stimulus_metadata['bg_ints']))
        background = {
            # rounded to first significant digit
            'bg_int': int(signif(np.sum(stimulus_metadata['bg_ints']), 1)), 
            'bg_name': stimulus_metadata['bg_name']
        }
        bg_dict.update(background)
        bg_entry = Background & background
        if len(bg_entry):
            bg_entry = bg_entry.fetch1()
            assert bg_dict == bg_entry, f"no match between {bg_dict} and {bg_entry}"
        else:
            Background.insert1(bg_dict, skip_duplicates=True)     
        
        reckey.update(key)
        reckey.update(background)
        reckey.update(stimulus_metadata[['total_start', 'total_end']].to_dict())
        reckey['times'] = times
        reckey['recording_time'] = pd.to_datetime(
            reckey['recording_time'], infer_datetime_format=True, 
            origin='unix'
        ).to_pydatetime()
        
        if reckey.get('brain_area', None) not in ['medulla', 'lobula']:
            cell_type = (CellTypes & reckey).fetch('cell_type')
            if not len(cell_type):
                warnings.warn(f"unknown cell type for genotype_id:\n{reckey}", UserWarning)
                cell_type = 'unknown'
            else:
                if len(cell_type) > 1:
                    warnings.warn(f"Multiple cell types for genotype_id:\n{reckey}", UserWarning)
                cell_type = cell_type[0]
            # TODO make regex
            # default brain areas
            if (
                cell_type.startswith('Tm') 
                or (cell_type == 'Ort')
                or (cell_type.startswith('LC'))
            ):
                reckey['brain_area'] = 'lobula'
            elif (
                cell_type.startswith('pR') 
                or cell_type.startswith('yR') 
                or (cell_type in ['Mi4', 'L3']) 
                or cell_type.startswith('yDm') 
                or cell_type.startswith('pDm') 
                or cell_type.startswith('Dm')
                or cell_type.startswith('ML1')
            ):
                reckey['brain_area'] = 'medulla'
            else:
                raise ValueError(
                    f"cell_type {cell_type} has "
                    "not default brain area for "
                    f"imaging - key:\n{reckey}."
                )
        self.insert1(reckey)
        
        for idx, row in roi_metadata.iterrows():
            idx = int(idx)
            row = row.to_dict()
            row['recording_id'] = reckey['recording_id']
            row['stimulus_set'] = reckey['stimulus_set']
            row['cell_id'] = idx
            row['dfof'] = dfof[:, idx]
            row['raw'] = raw[:, idx]
            
            self.Roi.insert1(row)
            
        for _, row in events.iterrows():
            row['recording_id'] = reckey['recording_id']
            row['stimulus_set'] = reckey['stimulus_set']
            
            row_events = row[['delay', 'recording_id', 'stimulus_set', 'dur', 'pause'] + LEDS].to_dict()
            
            self.Events.insert1(row_events)
            
            if name == 'gamut':
                row_events = row[[
                    'delay', 'recording_id', 'stimulus_set',
                    'color_space_id', 'metamer_id' 
                ]].to_dict()  
                self.GamutInfo.insert1(row_events)
                
            elif name == 'exploration':
                row_events = row[[
                    'delay', 'recording_id', 'stimulus_set',
                    'led_combos', 'diff', 
                    'led_number', 'is_off',
                ]].to_dict()  
                self.ExplorationInfo.insert1(row_events)
                
            elif name == 'ratio':
                row_events = row[[
                    'delay', 'recording_id', 'stimulus_set',
                    'led_combos', 
                    'led_combo1', 'led_combo2', 
                    'rel_abs_sum', 'ratio'
                ]].to_dict()  
                self.RatioInfo.insert1(row_events)
                
            else:
                raise ValueError(f'unknown name {name}')
                
                
@schema
class BackgroundCaptures(dj.Computed):
    definition = """
    -> Background
    -> SensitivitySet
    ---
    # absolute captures of background
    """ + "\n".join(
        f"{rh} : double  # absolute background capture"
        for rh in BG_QS
    ) + "\n" + "\n".join(
        f"{rh} : double  # absolute background variance"
        for rh in BG_QS_VAR
    )
    
    def make(self, key):
        bg_ints = load_bg_ints(key['bg_name'], key['bg_int'])
        
        bg_qs, bg_qs_var = calculate_capture(
            bg_ints, key['set_name'], return_var=True
        )
        
        # print(bg_qs, bg_qs_var)
        
        key.update(dict(zip(BG_QS, bg_qs)))
        key.update(dict(zip(BG_QS_VAR, bg_qs_var)))
        
        self.insert1(key)


@schema
class Captures(dj.Computed):
    definition = """
    -> Recording
    -> SensitivitySet
    ---
    """
    
    class Events(dj.Part):
        definition = """
        -> master
        -> Recording.Events
        ---
        # absolute captures of events
        """ + "\n".join(
            f"{rh} : double  # relative capture"
            for rh in QS
        ) + "\n" + "\n".join(
            f"{rh} : double  # relative variance"
            for rh in QS_VAR
        )
        
    def make(self, key):
        self.insert1(key)
        ints : pd.DataFrame = (Recording.Events & key).proj(*LEDS).fetch(format='frame')
        
        qs, qs_var = calculate_capture(
            ints.to_numpy(), key['set_name'], return_var=True
        )
        
        qs = pd.DataFrame(qs, index=ints.index, columns=QS)
        qs_var = pd.DataFrame(qs_var, index=ints.index, columns=QS_VAR)
        
        toinsert = pd.concat([qs, qs_var], axis=1, ignore_index=False).reset_index()
        toinsert['set_name'] = key['set_name']
        self.Events.insert(toinsert.to_dict('records'))


@schema
class Stats(dj.Computed):
    definition = """
    -> Recording
    ---
    """
        
    class Events(dj.Part):
        definition = """
        -> master
        -> Recording.Roi
        -> Recording.Events
        ---
        amp : double
        amp_off : double
        psth : longblob
        sig : tinyint
        sig_signed : smallint
        sig_off : tinyint
        sig_signed_off : smallint
        on_off : smallint
        """
        
    class Roi(dj.Part):
        definition = """
        -> master
        -> Recording.Roi
        ---
        est_var : double
        est_power : double
        est_power_off : double
        est_snr : double
        est_snr_off : double
        null_thresh : double
        auc : double
        auc_off : double
        ordered_r : longblob
        ordered_r_off : longblob
        """
    
    def make(self, key):
        self.insert1(key)
        events = (Recording.Events & key).fetch(format='frame').reset_index()
        t, total_start, total_end, rate = (Recording & key).fetch1(
            'times', 'total_start', 'total_end', 'rate'
        )
        
        cell_ids, signal = (Recording.Roi & key).fetch('cell_id', 'dfof')
        
        # sort stuff according to cell id
        argsort = np.argsort(cell_ids)
        cell_ids = cell_ids[argsort]
        signal = np.stack(signal[argsort], axis=-1)
        
        # smoothing of signal
        polyorder = 3
        smoothing_window = SMOOTHING_WINDOW * rate
        smoothing_window = int(np.floor(smoothing_window))
        window_length = (smoothing_window if (smoothing_window % 2) == 1 else smoothing_window + 1)
        signal = savgol_filter(signal, window_length, polyorder, axis=0)
        
        # this is still working in seconds
        # interpolator
        interp = interp1d(
            t, signal, axis=0, 
            bounds_error=False, 
            fill_value=0
        )
        
        # events x time x rois
        sinterp = interp(
            TINTERP 
            + events['delay'].to_numpy()[:, None]
        )
        
        # get the null psth distribution 
        # events x time x rois
        null_delays = np.concatenate([
            np.linspace(
                total_start - NULL_T_BEFORE - NULL_BEFORE_OFFSET, 
                total_start - NULL_BEFORE_OFFSET, NULL_S_BEFORE), 
            np.linspace(total_end, total_end + NULL_T_AFTER, NULL_S_AFTER)
        ])
        null_interp = interp(TINTERP + null_delays[:, None])
        
        # reformat
        # events x time x rois
        times = np.broadcast_to(TINTERP[:, None], sinterp.shape[1:]).reshape(-1)
        rois = np.broadcast_to(cell_ids[None, :], sinterp.shape[1:]).reshape(-1)
        # events x (time x rois)
        sinterp = sinterp.reshape(sinterp.shape[0], -1)
        null_interp = null_interp.reshape(null_interp.shape[0], -1)
        
        # new columns indices and row indices
        columns = pd.MultiIndex.from_arrays([times, rois], names=['time', 'cell_id'])
        index = pd.MultiIndex.from_frame(events[['delay']])
        
        # reformat PSTH
        df = pd.DataFrame(sinterp, index=index, columns=columns).stack(level='cell_id')
        df_arr = df.to_numpy(copy=False)
        # create final dataframe
        df = df.index.to_frame(False)
        df['psth'] = list(df_arr)  # this will be saved
        
        null_df = pd.DataFrame(
            null_interp, 
            index=pd.Index(np.arange(null_interp.shape[0]), name='null_sample_id'), 
            columns=columns
        ).stack(level='cell_id')
        # create null df for stat calculation
        null_df_arr = null_df.to_numpy(copy=False)
        null_df = null_df.index.to_frame(False)
        
        # calculate amplitudes
        tbool_amp = TBOOL_AMP
        tbool_baseline = TBOOL_BASELINE
        tbool_off =  TBOOL_OFF
                
        df['amp'] = df_arr[:, tbool_amp].mean(axis=-1) - df_arr[:, tbool_baseline].mean(axis=-1)
        df['amp_off'] = df_arr[:, tbool_off].mean(axis=-1) - df_arr[:, tbool_baseline].mean(axis=-1)
        # null distribution for amplitudes
        null_df['amp'] = null_df_arr[:, tbool_amp].mean(axis=-1) - null_df_arr[:, tbool_baseline].mean(axis=-1)
                
        # threshold to assess if a response is significantly different from zero
        null_thresh = null_df.groupby(['cell_id'])['amp'].apply(lambda x: np.percentile(np.abs(x), q=CI))
        null_thresh.name = 'null_thresh'
        null_thresh = null_thresh.reset_index()
        info = null_thresh  # info dataframe
        
        # merge df with null threshold to calculate stats for each event
        df = pd.merge(df, null_thresh)
        df['sig'] = np.abs(df['amp']) > df['null_thresh']
        df['sig_signed'] = df['sig'] * np.sign(df['amp'])
        df['sig_off'] = np.abs(df['amp_off']) > df['null_thresh']
        df['sig_signed_off'] = df['sig_off'] * np.sign(df['amp_off'])
        df['on_off'] = df['sig_signed'] * df['sig_signed_off']
        # drop null threshold column again
        df.drop(columns=['null_thresh'], inplace=True)
        
        # calculte estimate variance in response from null distribution
        est_var = null_df.groupby(['cell_id'])['amp'].apply(lambda x: np.sum(x**2)/(len(x) - 1))
        est_var.name = 'est_var'
        est_var = est_var.reset_index()
        # print(est_var)
        info = pd.merge(info, est_var)  # merge
        
        # calculate estimated power of responses
        est_power = df.groupby(['cell_id'])['amp'].apply(calculate_dynamic_range)
        est_power.name = 'est_power'
        est_power = est_power.reset_index()
        # print(est_power)
        info = pd.merge(info, est_power)
        
        # calculate estimated power of responses
        est_power = df.groupby(['cell_id'])['amp_off'].apply(calculate_dynamic_range)
        est_power.name = 'est_power_off'
        est_power = est_power.reset_index()
        info = pd.merge(info, est_power)
        
        info['est_snr'] = info['est_power'] / np.maximum(info['est_var'].to_numpy(), 1e-8)
        info['est_snr_off'] = info['est_power_off'] / np.maximum(info['est_var'].to_numpy(), 1e-8)
        
        # percentile cutoffs for amp - true positive rate and false positive rate
        # true positive rate = (true positive) / (true positives + false negatives) - amp percentile
        # false positive rate = (false positive) / (false positive + true negatives) - null percentile
        roc_dict = {}
        for cell_id in cell_ids:
            # calculate sparsity index
            amps = df[df['cell_id'] == cell_id][['amp', 'amp_off']].to_numpy()
            
            # sparsity analysis
            _, y, auc = calculate_sparsity(amps.T, return_all=True)
            assert (y.ndim == 2) and (y.shape[0] == 2), 'sanity check failed'
            
            # add to info
            roc_dict[cell_id] = pd.Series({
                'ordered_r': y[0], 
                'ordered_r_off': y[1], 
                'auc': float(auc[0]), 
                'auc_off': float(auc[1])
            })
            
        roc_df = pd.DataFrame.from_dict(roc_dict, orient='index')
        roc_df.index.name = 'cell_id'
        roc_df = roc_df.reset_index()
        length = len(info)
        info = pd.merge(info, roc_df, on=['cell_id'])
        assert len(info) == length, "sanity length check"
        
        # update with info from 
        for k, v in key.items():
            df[k] = v
            info[k] = v
                        
        self.Events.insert(df.to_dict('records'))
        self.Roi.insert(info.to_dict('records'))
