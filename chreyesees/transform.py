from scipy.special import factorial
import numpy as np
import torch
from dreye.api import barycentric
from itertools import combinations_with_replacement, combinations
# transformations for running models

bigA = np.array([
    [1, 1, 1, 1], 
    [1, 1, -1, -1], 
    [-1, 1, 1, -1], 
    [1, -1, 1, -1]
]).T

def identity(X):
    return X


def contrast(X):
    return X - 1


def excitation_pca(X, n=1, reduced=None):
    assert X.shape[1] == 4
    X = excitation_contrast(X, n=n)
    A = bigA
    X = X @ A
    if reduced is not None:
        X = X[:, reduced]
    return X


def log_pca(X, thresh=1e-1, reduced=None):
    assert X.shape[1] == 4
    X = log_contrast(X, thresh=thresh)
    A = bigA
    X = X @ A
    if reduced is not None:
        X = X[:, reduced]
    return X

def inverse_log_pca(X, thresh=1e-1):
    assert X.shape[1] == 4
    A = np.linalg.inv(bigA)
    X = X @ A
    X = inverse_log_contrast(X, thresh=thresh)
    return X


def log_contrast(X, thresh=1e-1, additive=True):
    if additive:
        X = (X + thresh)/(1 + thresh)
    else:
        X = X.copy()
        X[X < thresh] = thresh
    try:
        return np.log(X)
    except TypeError:
        return torch.log(X)
    

def inverse_log_contrast(X, thresh=1e-1, additive=True):
    X = np.exp(X)
    if additive:
        X = X * (1 + thresh) - thresh
    else:
        X = X - thresh
    return X