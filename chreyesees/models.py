import inspect
from itertools import product
from numbers import Number
import numpy as np
import pandas as pd
import torch
from scipy.spatial import Delaunay
from dreye.api.convex import in_hull
from sklearn.linear_model import LinearRegression
from scipy.optimize import least_squares
from chreyesees.rbf.rbfinterp import RbfModel

from sklearn.model_selection import KFold
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn import clone
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, DotProduct, WhiteKernel, ConstantKernel
from sklearn.metrics import r2_score
# from torch import nn
from chreyesees import utils
from dreye.api import barycentric
# import jax.numpy as jnp

from .lstsq import least_squares as torch_least_squares
from . import transform


# redundant but used
LinearRegression = LinearRegression
RbfModel = RbfModel


def tanh_like(x, r0, a=1, module=np):
    """
    tanh-like function from Rajan, Abbott, Sompolinsky (2010).
    """
    tanh = getattr(module, 'tanh')
    zeros_like = getattr(module, 'zeros_like')
    y = zeros_like(x)

    y[x <= 0] = (1 - r0) * tanh(x[x <= 0] / np.maximum(1 - r0, 1e-6))
    y[x > 0] = (1 + r0) * tanh(x[x > 0] / np.maximum(1 + r0, 1e-6))

    return a * y

def clip(x, lower, upper, module=np, tanh=False):
    if lower is None and upper is None:
        return x
    elif not tanh:
        method = getattr(module, 'clip')
        return method(x, lower, upper)
    else:
        # method = getattr(module, 'tanh')
        scaler, offset = get_tanh_vars(lower, upper)
        return tanh_like(x, r0=offset, a=scaler, module=module)
        # return scaler * (method(x - offset) + np.tanh(offset))
    
    
def get_tanh_vars(lower, upper):
    lower = np.minimum(lower, -1e-3)
    upper = np.maximum(upper, 1e-3)
    scaler = (upper - lower) / 2
    offset = lower / scaler + 1
    return scaler, offset


class ThresholdLinear(LinearRegression):
    
    def __init__(
        self, fit_intercept=True, 
        normalize="deprecated", 
        copy_X=True, n_jobs=None, positive=False, 
        threshold_y=None, transform_func=None, 
        tanh=False, refit=None
    ):
        super().__init__(
            fit_intercept=fit_intercept, 
            normalize=normalize, copy_X=copy_X, 
            n_jobs=n_jobs, positive=positive
        )
        self.threshold_y = threshold_y
        self.transform_func = transform_func
        self.tanh = tanh
        self.refit = refit
        
    def _transform_func(self, X):
        if self.transform_func is not None:
            if isinstance(self.transform_func, str):
                X = getattr(transform, self.transform_func)(X)
            else:
                X = self.transform_func(X)
        return X
    
    def _extract_params(self, x0):
        n = 0
        if self.refit:
            if self.refit == 'threshold':
                coef = self.coef_
                intercept = self.intercept_
            else:
                n = self.n_features_in_
                coef = x0[:n]
                if self.fit_intercept:
                    intercept = x0[n:n+1]
                    n += 1
                else:
                    intercept = 0
        else:
            coef = self.coef_
            intercept = self.intercept_
        ylower, yupper = x0[n:]
        return coef, intercept, ylower, yupper
    
    def _objective(self, x0, X, y, sample_weight):
        coef, intercept, ylower, yupper = self._extract_params(x0)
        
        ypred = clip(X @ coef + intercept, ylower, yupper, tanh=self.tanh)
        if sample_weight is None:
            return y - ypred
        else:
            return np.sqrt(sample_weight) * (y - ypred)
        
        
    def fit(self, X, y, sample_weight=None):
        X = self._transform_func(X) 
        super().fit(X, y, sample_weight=sample_weight)
        
        if self.threshold_y is not None:
            lower, upper = self.threshold_y
            if isinstance(lower, Number) and isinstance(upper, Number):
                if lower is not None:
                    ylower = float(np.percentile(y, q=lower))
                else:
                    ylower = None
                if upper is not None:
                    yupper = float(np.percentile(y, q=upper))
                else:
                    yupper = None
                self.ylower_ = ylower
                self.yupper_ = yupper
            else:
                # dynamically find the best threshold
                if isinstance(lower, Number):
                    lower = [lower]
                if isinstance(upper, Number):
                    upper = [upper]
                    
                ypred = super().predict(X)
                
                ylowers, yuppers, r2s = [], [], []
                for lower_, upper_ in product(lower, upper):
                    ylower = float(np.percentile(y, q=lower_))
                    yupper = float(np.percentile(y, q=upper_))
                    ylowers.append(ylower)
                    yuppers.append(yupper)
                    
                    # fit best scalar
                    r2s.append(
                        r2_score(
                            y, clip(ypred, ylower, yupper, tanh=self.tanh), 
                            sample_weight=sample_weight
                        )
                    )
                    
                assert len(ylowers) == len(yuppers) == len(r2s)
                idx = np.argmax(r2s)
                self.ylower_ = ylowers[idx]
                self.yupper_ = yuppers[idx]
                
            if self.refit is None:
                self.ylower_, self.yupper_ = np.minimum(self.ylower_, -1e-3), np.maximum(self.yupper_, 1e-3)
            elif self.refit is not None:
                if self.refit == 'all':
                    x0 = self.coef_
                    if self.fit_intercept:
                        x0 = np.concatenate([x0, [self.intercept_]])
                    ylower, yupper = np.minimum(self.ylower_, -1e-3), np.maximum(self.yupper_, 1e-3)
                    x0 = np.concatenate([x0, [ylower, yupper]])
                    
                    results = least_squares(
                        self._objective, 
                        x0=x0, 
                        bounds=(
                            np.array([-np.inf]*len(x0[:-2])+[-np.inf, 0]), 
                            np.array([np.inf]*len(x0[:-2])+[0, np.inf])
                        ), 
                        args=(X, y, sample_weight)
                    )
                    self.coef_, self.intercept_, self.ylower_, self.yupper_ = self._extract_params(results.x)
                elif self.refit == 'threshold':
                    ylower, yupper = np.minimum(self.ylower_, -1e-3), np.maximum(self.yupper_, 1e-3)
                    x0 = np.array([ylower, yupper])
                    
                    results = least_squares(
                        self._objective, 
                        x0=x0, 
                        bounds=(
                            np.array([-np.inf, 0]), 
                            np.array([0, np.inf])
                        ), 
                        args=(X, y, sample_weight)
                    )
                    self.coef_, self.intercept_, self.ylower_, self.yupper_ = self._extract_params(results.x)
                                        
                # pass
        else:
            self.ylower_ = None
            self.yupper_ = None
        
        return self
        
    def predict(self, X, do_clip=True):
        X = self._transform_func(X)
        ypred = super().predict(X)
        if do_clip:
            return clip(ypred, self.ylower_, self.yupper_, tanh=self.tanh)
        return ypred


class SelectivityModel(BaseEstimator, RegressorMixin):
    
    def __init__(
        self, 
        kappas=np.logspace(-3, 1, 5+4*5), 
        # kappas=np.linspace(-1, 1, 21),
        # alphas=np.linspace(-10, 10, 21),
        alphas=np.logspace(-1, 1, 3+2*5), 
        linear_features=None,
        eps=1e-3, 
        cv=None,
    ):
        self.eps = eps
        self.kappas = kappas
        self.alphas = alphas
        self.linear_features = linear_features
        self.cv = cv

    def _selective_regression(self, X_normalized, X_norm, w_normalized, w_norm, alpha, kappa):
        # TODO other regression methods
        angle = X_normalized @ w_normalized
        if np.abs(kappa) <= self.eps:
            return (
                X_norm ** alpha
                * 
                w_norm 
                * 
                angle
            )
        return (
            X_norm ** alpha
            * 
            w_norm 
            * 
            (np.exp(kappa * angle) - 1) 
            / kappa
        )
        
    def _model(self, theta, X, X_norm, a, k):
        w = theta[self.n_linear_:]
        w_norm = np.linalg.norm(w)
        if np.isclose(w_norm, 0):
            w_normalized = w
        else:
            w_normalized = w / w_norm
        y_pred = self._selective_regression(
            X[:, self.n_linear_:], X_norm, w_normalized, w_norm, a, k
        ) + X[:, :self.n_linear_] @ theta[:self.n_linear_]
        return y_pred

    def _residuals(self, theta, X, X_norm, y, a, k, sample_weight):
        y_pred = self._model(theta, X, X_norm, a, k)
        return np.sqrt(sample_weight) * (y - y_pred)
    
    def _check_and_assign_params(self):
        if self.linear_features is None:
            self.linear_features_ = []
            self.n_linear_ = 0
        else:
            self.linear_features_ = (
                [self.linear_features] 
                if isinstance(self.linear_features, Number) 
                else list(self.linear_features)
            )
            self.n_linear_ = len(self.linear_features_)
    
    def _transformX(self, X, return_nonnormalized=False):
        idcs = list(range(X.shape[1]))
        idcs = list(set(idcs) - set(self.linear_features_))
        Xlinear = X[:, self.linear_features_]
        X = X[:, idcs]
        if return_nonnormalized:
            return np.hstack([Xlinear, X])
        
        xnorm = np.linalg.norm(X, axis=-1, ord=2)
        X = X.copy()
        X[xnorm != 0] /= xnorm[xnorm != 0, None]
        X[~np.isfinite(X)] = 0
        
        X = np.hstack([Xlinear, X])
        return X, xnorm
    
    def fit(self, X, y, sample_weight=None):
        
        self._check_and_assign_params()
        
        X4linear = self._transformX(X, return_nonnormalized=True)
        lin_model = LinearRegression(fit_intercept=False)
        lin_model.fit(X4linear, y, sample_weight=sample_weight)
        self.lin_model_ = lin_model
        
        self.w_linear_ = lin_model.coef_
        
        X, X_norm = self._transformX(X)
        sample_weight = (np.ones_like(y) if sample_weight is None else sample_weight)
        
        if self.cv is None:
            params = []
            r2s = []
            for a, k in product(self.alphas, self.kappas):
                result = least_squares(
                    self._residuals, self.w_linear_, 
                    args=(X, X_norm, y, a, k, sample_weight), 
                )
                test_y = self._model(
                    result.x, X, X_norm, a, k
                )
                r2s.append(utils.nan_r2_score(y, test_y, sample_weight=sample_weight))
                params.append([result.x, a, k])
        else:
            kfold = KFold(n_splits=self.cv, shuffle=True)
            params = []
            r2s = []
            for a, k in product(self.alphas, self.kappas):
                test_r2 = 0
                coef = 0
                for (train_idx, test_idx) in (kfold.split(X)):
                    X_train = X[train_idx]
                    X_test = X[test_idx]
                    y_train = y[train_idx]
                    y_test = y[test_idx]
                    X_norm_train = X_norm[train_idx]
                    X_norm_test = X_norm[test_idx]
                    w_train = sample_weight[train_idx]
                    w_test = sample_weight[test_idx]
                    
                    result = least_squares(
                        self._residuals, self.w_linear_, 
                        args=(X_train, X_norm_train, y_train, a, k, w_train), 
                    )
                    test_y = self._model(
                        result.x, X_test, X_norm_test, a, k
                    )
                    test_r2 += utils.nan_r2_score(y_test, test_y, sample_weight=w_test)
                    coef = coef + result.x
                    
                params.append([coef / self.cv, a, k])
                r2s.append(test_r2 / self.cv)
                
        self.r2s_ = np.array(r2s)
        self.params_ = params
        
        self.coef_, self.alpha_, self.kappa_ = params[np.argmax(r2s)]
        
        # backwards compatibility
        self.tau_ = np.array([self.kappa_, self.alpha_])
        self.intercept_ = 0
        
    def predict(self, X):
        X, X_norm = self._transformX(X)
        return self._model(self.coef_, X, X_norm, self.alpha_, self.kappa_)
    
    def get_linear_component(self, X):
        X, _ = self._transformX(X)
        theta = self.coef_
        return X[:, :self.n_linear_] @ theta[:self.n_linear_]
        
    def xnorm_predict(self, X, y):
        linear_component = self.get_linear_component(X)
        X, X_norm = self._transformX(X)
        theta = self.coef_
        w = theta[self.n_linear_:]
        w_norm = np.linalg.norm(w)
        w_normalized = w / w_norm
        y = (y - linear_component) / (w_norm * X_norm ** self.alpha_)
        x = X[:, self.n_linear_:] @ w_normalized
        return y, x
    
    def x_predict(self, X, y):
        linear_component = self.get_linear_component(X)
        X, X_norm = self._transformX(X)
        theta = self.coef_
        w = theta[self.n_linear_:]
        w_norm = np.linalg.norm(w)
        w_normalized = w / w_norm
        
        y = (y - linear_component) / self._selective_regression(
            X[:, self.n_linear_:], X_norm, w_normalized, w_norm, 0, self.kappa_
        )
        
        return y, X_norm

