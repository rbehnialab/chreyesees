"""Defining various color palettes
to consistently plot

Take a look at proplot's color for reference
"""

from chreyesees import OPSINS
from chreyesees import LEDS_CMAP
from proplot import to_rgb
import numpy as np
import seaborn as sns


class ExtraDict(dict):
    
    def __getitem__(self, key):
        try:
            return super().__getitem__(key)
        except KeyError as e:
            possibles = [k for k in self if key.startswith(k)]
            if len(possibles) != 1:
                raise e
            return super().__getitem__(possibles[0])

CMAP_OPSINS = ExtraDict({
    'rh1': 'gray',
    'rh3': 'magenta', 
    'rh4': 'barney', 
    'rh5': 'blue', 
    'rh6': 'green',
})

CMAP_LEDS = LEDS_CMAP # alias

def desaturate(rgb):
    # lesum = sum(rgb)
    rgb = [(c+0.5)/(1.5) for c in rgb]
    return tuple(rgb)
    

# flatui = sns.color_palette('FlatUI', 6)

CMAP_CELLS = ExtraDict({
    'pR7': 'magenta', 
    'yR7': 'barney', 
    'pR8': 'blue', 
    'yR8': 'green',
    'yDm8': 'goldenrod', 
    'pDm8': 'mushroom', 
    'Tm5b': 'violet9', #desaturate(to_rgb('magenta')), 
    'Tm5a': 'pink9',# desaturate(to_rgb('barney')),
    'Tm20': 'cyan9', #desaturate(to_rgb('blue')),
    'Tm5c': 'gray9', #desaturate(to_rgb('green')),
})

CMAP_STIM_SET = {'exploration': 'gray', 'gamut': 'tomato'}