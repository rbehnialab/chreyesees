"""
Utility functions
"""

from itertools import product
from numbers import Number
import numpy as np
import cvxpy as cp
from numpy.random import default_rng
from scipy import linalg
import pandas as pd
import puffbird as pb
import torch
import os
from dreye.api import spherical
from dreye.api.convex import in_hull
from scipy.ndimage import gaussian_filter1d
from scipy.interpolate import interp1d
from dreye.datasets import load_flowers, load_spitschan2016
from dreye import integral, ReceptorEstimator
from sklearn.cluster import KMeans
from sklearn.model_selection import RepeatedKFold
from statsmodels.multivariate.pca import PCA as statsPCA
from tqdm import tqdm
from sklearn.decomposition import PCA
from dreye.api import barycentric, spherical
from joblib import Parallel, delayed
from sklearn.metrics import r2_score
from sklearn.preprocessing import normalize, minmax_scale

from . import BG_QS, BG_QS_VAR, LEDS, MASTER_FOLDER, WLS, BG_LEDS


def twod_transform(q, divmean=True):
    # 2d opponent space
    A = np.array([
        [1, 1, -1, -1], 
        [-1, 1, 1, -1]
    ]).T
    
    if divmean:
        q = q / np.mean(q, axis=-1, keepdims=True)
    q = q - 1
    # q = (q - 1)/(q + 1)
    # q = np.log((x+1e-3)/(1+1e-3))
    return q @ A


def rayleigh_vec(counts, X, ymean):
    # X = X[:, [1, 2]]
    # only 2 opponent dimensions
    isnull = np.isnan(ymean)
    norm = np.linalg.norm(X, axis=1)
    norm = minmax_scale(norm)
    
    # for thresh in np.linspace(0.1, 1, 10):
    nbool = norm <= 0.5
    
    X = X[~isnull & nbool]
    ymean = ymean[~isnull & nbool]
    counts = counts[~isnull & nbool]
    
    X = normalize(X, norm='l2', axis=1)
    xvec = np.sum(
        counts[:, None] * X * ymean[:, None], axis=0
    ) / np.sum(
        counts * np.abs(ymean) #* norm
    )
    return xvec
        
        
def rayleigh_value(counts, X, ymean):
    return np.linalg.norm(rayleigh_vec(counts, X, ymean))



def pca_complete(X):
    cov = X.T @ X
    eigval, eigvec = np.linalg.eig(cov)
    argsort = np.argsort(eigval)[::-1]
    eigval = eigval[argsort]
    eigval = eigval / np.sum(eigval)
    eigvec = eigvec[:, argsort]
    return eigval, eigvec


def pca_weighted_counts(X, *counts, demean=True, alpha=1):
    if demean:
        X = X - np.nanmean(X, axis=0)
    _, eigvec = pca_complete(X)
    Xt = X @ eigvec
    return tuple((alpha * count) / np.linalg.norm(Xt, axis=-1) for count in counts)


def equalize_data_dist(
    X, *args, return_repeats=False, return_idcs=False, 
    demean=True
):
    # equalize data distribution as much as possible
    if demean:
        X = X - np.nanmean(X, axis=0)
    eigval, eigvec = pca_complete(X)
    Xt = X @ eigvec
    repeats = 1 / np.linalg.norm(Xt, axis=-1)
    # repeats = (
    #     1/(np.abs(Xt)*eigval)
    # ).sum(-1)
    repeats = repeats / np.min(repeats)
    repeats = repeats.astype(int)
    Xrep = np.repeat(X, repeats, axis=0)
    args_rep = []
    for arg in args:
        args_rep.append(np.repeat(arg, repeats, axis=-1))
    output = tuple([Xrep] + args_rep)
    if return_repeats and return_idcs:
        return output, repeats, np.repeat(np.arange(X.shape[0]), repeats, axis=0)
    elif return_idcs:
        return output, np.repeat(np.arange(X.shape[0]), repeats, axis=0)
    elif return_repeats:
        return output, repeats
    return output


def nan_r2_score(y_true, y_pred, *, sample_weight=None, **kwargs):
    # r2 score mainly for radius neighbor regressor
    y_true = np.asarray(y_true)
    y_pred = np.asarray(y_pred)
    assert y_true.ndim == 1, 'For nan_r2_score, y_true must be one-dimensional.'
    isfinite = np.isfinite(y_true) & np.isfinite(y_pred)
    if sample_weight is not None:
        sample_weight = sample_weight[isfinite].copy()
    return r2_score(y_true[isfinite].copy(), y_pred[isfinite].copy(), sample_weight=sample_weight, **kwargs)


def _random_nan_choice(y, rng):
    y = y.copy()
    isfinite = np.isfinite(y)
    y[~isfinite] = rng.choice(
        y[isfinite], replace=True, size=(~isfinite).sum()
    )
    return y

def calculate_sparsity(y, return_all=True, axis=-1, seed=None, remove_outliers=None):
    """Calculate sparsity index
    """
    # calculate sparsity
    if remove_outliers is not None:
        lower, upper = np.nanpercentile(y, q=((100-remove_outliers)/2, (remove_outliers+100)/2), axis=axis, keepdims=True)
        y = np.where(
            (y > lower) & (y < upper), 
            y, 
            np.ones_like(y) * np.nan
        )
    mask = np.isnan(y)
    size = y.shape[axis]
    # substitute nans with random values in array
    if np.any(mask):
        rng = np.random.default_rng(seed)
        y = np.apply_along_axis(
            _random_nan_choice,
            axis, 
            y,
            rng=rng
        )
        assert np.all(np.isfinite(y)), "all must be finite"
    y = np.abs(y)
    ymax = np.max(y, axis=axis, keepdims=True)
    y = y / np.maximum(ymax, 1e-8)
    y = np.sort(y, axis=axis)
    percentiles = np.broadcast_to(np.linspace(0, 1, size), y.shape)
    auc = np.trapz(percentiles, y)
    if return_all:
        return percentiles, y, auc
    return auc
    

def _calculate_invariance_helper(angles, X, y, quadrant_vec, similarity):
    vec_sph = np.concatenate([[1], angles])[None]
    vector = spherical.spherical_to_cartesian(vec_sph)[0]
    x = X @ vector
    # assumes l2 normalization - which is the case for the spherical calculation with radii 1
    expl_var = np.var(x, axis=0)
    r = np.abs(nan_r2er_score(x, y))
    is_invariant = np.inner(vector, quadrant_vec) >= similarity
    return np.array([r, is_invariant, expl_var], dtype=object)


def calculate_invariance(
    X, y, n_samples=5, quadrant_vec=None, n_jobs=12, method='mean', return_infos=False, 
    similarity=np.cos(np.deg2rad(11.25)), solo=True, flip=True, 
    equalize=True,
):
    """Calculate invariance in some "quadrant" in multi-d space
    """
    if equalize:
        X, y = equalize_data_dist(X, y)
    if flip:
        X = np.vstack([X, -X])
        y = np.concatenate([y, -y], axis=-1)
    n = X.shape[1]
    full_var = np.var(X, axis=0).sum()
    if quadrant_vec is None:
        quadrant_vec = np.ones(n) / np.linalg.norm(np.ones(n))
    else:
        quadrant_vec = quadrant_vec / np.linalg.norm(quadrant_vec)
    assert n >= 2, "at least two dimensions necessary, but only 1."
    angles = [np.linspace(0, np.pi, n_samples)]*(n-2) + [np.linspace(0, 2*np.pi, 2*(n_samples-1)+1)[:-1]]
    # parallelize
    if n_jobs is None:
        r, is_invariant, expl_var = np.stack([
            _calculate_invariance_helper(iangles, X, y, quadrant_vec, similarity)
            for iangles in tqdm(product(*angles), desc='invariance', total=int(np.prod([len(angle) for angle in angles])))
        ], axis=1)
    else:
        r, is_invariant, expl_var = np.stack(Parallel(n_jobs=n_jobs)(
            delayed(_calculate_invariance_helper)(iangles, X, y, quadrant_vec, similarity)
            for iangles in product(*angles)
        ), axis=1)
        
    if solo:
        x_ = X @ quadrant_vec
        # assumes l2 normalization - which is the case for the spherical calculation with radii 1
        expl_var_ = np.var(x_, axis=0)
        r_ = np.abs(nan_r2er_score(x_, y))
        r = np.concatenate([[r_], np.stack(r)], axis=0).T
        is_invariant = np.concatenate([[1], is_invariant])
        expl_var = np.concatenate([[expl_var_], expl_var])
        
    expl_var_ratio = expl_var / full_var
        
    is_invariant = is_invariant.astype(int).astype(bool)
    if method == 'mean':
        v = np.mean(r[..., is_invariant], axis=-1)/(np.mean(r, axis=-1))
    elif method  == 'max':
        v = np.max(r[..., is_invariant], axis=-1)/(np.max(r[..., ~is_invariant], axis=-1))
    elif method == 'weighted-mean':
        r_ = r * (1/expl_var_ratio)/np.sum(1/expl_var_ratio)
        v = np.mean(r_[..., is_invariant], axis=-1)/(np.mean(r_, axis=-1))
    elif method == 'weighted-max':
        r_ = r * (1/expl_var_ratio)/np.sum(1/expl_var_ratio)
        v = np.max(r_[..., is_invariant], axis=-1)/(np.max(r_, axis=-1))
    else:
        raise ValueError(f"`method` {method} unknown.")
    
    if return_infos:
        return v, expl_var_ratio, is_invariant
    return v

def variance_stabilizing_transform(
    X, 
    xstd_constant=None,
    constant_cutoff=0.1, 
    minimum_fraction=0.1
):
    """
    Variance stabilizing transform across stimuli.

    Parameters
    ----------
    X : numpy.ndarray (n_stimuli, n_samples)
        Array of a number of responses to various stimuli
        from different samples (i.e. ROIs/neurons)
    constant_cutoff : float [0, 1]
        Percentile from the 
        standard deviations calculated across samples
        used as a cutoff for the constant std.
    minimum_fraction : float [0, 1]
    """
    xstd = np.nanstd(X, axis=-1, keepdims=True)
    if xstd_constant is None:
        xstd_constant = np.nanpercentile(xstd, q=constant_cutoff*100)
    xstd = np.clip(
        xstd - xstd_constant, xstd_constant * minimum_fraction, 
        None
    )

    return (
        np.sign(X) 
        * 1 / xstd 
        * np.arcsinh(np.abs(X) / (xstd_constant / xstd))
    )
    
    
def rough_sphere_packing(pts, radius):
    """roughly pack sphere given the convex hull given by the points
    """
    mini, maxi = np.min(pts), np.max(pts)
    
    steps = np.arange(mini, maxi+radius, 2*radius)
    steps = np.array(list(product(steps, repeat=pts.shape[1])))
    
    inhull = in_hull(pts, steps)
    return steps[inhull]
    

def signif(x, p=1):
    """
    Round an array to a significant digit. 

    Parameters
    ----------
    x : numpy.ndarray
        Array to round
    p : int
        Number of digits to round to

    Returns
    -------
    x : numpy.ndarray
        Rounded array
    """
    x = np.asarray(x)
    x_positive = np.where(np.isfinite(x) & (x != 0), np.abs(x), 10**(p-1))
    mags = 10 ** (p - 1 - np.floor(np.log10(x_positive)))
    return np.round(x * mags) / mags


def get_module(module='numpy'):
    if module == 'numpy':
        module = np
    elif module == 'torch':
        module = torch
    else:
        raise ValueError(f"module `{module}` unknown.")
    return module


def _r2er_score(y, ypred, counts, module, axis, clamps=None):
    counts = (
        module.ones_like(y) 
        if counts is None 
        else module.broadcast_to(counts, y.shape)
    )
    var1 = module.sum(counts * y ** 2, axis=axis)
    if clamps is not None:  # assume only used with torch and inplace
        # no_grad
        with torch.no_grad():
            var2 = module.sum(counts * ypred ** 2, axis=axis)
            clamps0 = clamps[0] * var2 / var1
            clamps0[~torch.isfinite(clamps0)] = ypred.min()
            clamps1 = clamps[1] * var2 / var1
            clamps1[~torch.isfinite(clamps1)] = ypred.max()
        ypred.clamp_(clamps0, clamps1)
        var2 = module.sum(counts * ypred ** 2, axis=axis)
    else:
        var2 = module.sum(counts * ypred ** 2, axis=axis)
    cov = module.sum(counts * y * ypred, axis=axis)
    # sign preserving r2
    return (cov**3/module.clip(module.abs(cov), 1e-8, None)) / module.clip(var1 * var2, 1e-8, None)


def r2er_score(y, ypred, axis=-1, module='numpy', counts=None, clamps=None):
    """Non-centered r2 score
    """
    module = get_module(module)
    if y.ndim > ypred.ndim:
        ypred = module.broadcast_to(ypred, y.shape)
    elif ypred.ndim > y.ndim:
        y = module.broadcast_to(y, ypred.shape)
    return _r2er_score(y, ypred, counts, module, axis, clamps)

def nan_r2er_score(y, ypred, axis=-1, module='numpy', counts=None, inplace=False, clamps=None):
    """Non-centered r2 score with nans
    """
    module = get_module(module)
    if y.ndim > ypred.ndim:
        ypred = module.broadcast_to(ypred, y.shape)
    elif ypred.ndim > y.ndim:
        y = module.broadcast_to(y, ypred.shape) 
        
    if not inplace:
        if hasattr(y, 'copy'):
            y = y.copy()
            ypred = ypred.copy()
        else:
            y = y.clone()
            ypred = ypred.clone()
  
    isnan = module.isnan(y) | module.isnan(ypred)
    # set all nan possibilities to zero
    y[isnan] = 0
    ypred[isnan] = 0
    return _r2er_score(y, ypred, counts, module, axis, clamps)


def r2er_n2m(y, ypred, var, n=1):
    """
    Neuron to model approximated unbiased estimator of r2er - noise correction.

    Parameters
    ----------
    y : numpy.ndarray (n_neurons, n_stimuli)
    ypred : numpy.ndarray (n_stimuli)
    var : numpy.ndarray, optional
    center : bool, optional
    n : int, optional
    """
    if isinstance(n, np.ndarray):
        m = n
        n = np.mean(n)
    else:
        m = 1
        
    ypred[np.isnan(y)] = np.nan

    cov = np.nansum(m * y * ypred, axis=-1)
    xy2 = cov ** 2
    var1 = np.nansum(m * y ** 2, axis=-1)
    var2 = np.nansum(m * ypred ** 2, axis=-1)
    x2y2 = var1 * var2
    
    # unbiased numerator and denominator
    ub_xy2 = xy2 - var / n * var2
    ub_x2y2 = x2y2 - (np.nansum(m) - 1) * var / n * var2
    
    # form ratio of individually unbiased estimates from r^2_er
    hat_r2er = ub_xy2 / ub_x2y2
    return hat_r2er


def calculate_dynamic_range(Y, center=True):
    """
    Approximate dynamic range of each neuron.
    SNR =  (d2 / estimated variance) assuming homoscedasticity.

    Parameters
    ----------
    Y : numpy.ndarray
        N trials x M neurons

    Returns
    -------
    d2 : numpy.ndarray
        Estimate of dynamic range
    """
    
    # power of responses
    if center:
        Y = Y - np.nanmean(Y, axis=0, keepdims=True)
        
    d2 = np.nansum(Y**2, 0) / (np.isfinite(Y).sum(0) - 1)
    return d2


def load_led_spectra(wls=WLS):
    """Load normalized spectra of LEDs

    Parameters
    ----------
    wls : numpy.ndarray
        Wavelength values to interpolate to.

    Returns
    -------
    led_spectra: numpy.ndarray of shape (wls, leds)
        LEDs are in order of [duv, uv, violet, rblue, lime, orange]
    """
    filepath = os.path.join(
        MASTER_FOLDER, 'data', 'flux_normalized_led_spectra.json', 
    )
    nsdf = pd.read_json(filepath, orient='split')
    led_spectra = nsdf.to_numpy()
    wls_old = nsdf.index
    # interpolate to desired wavelengths
    led_spectra = interp1d(wls_old, led_spectra, axis=0)(wls)
    # normalized led spectra
    return led_spectra / integral(led_spectra, wls, axis=0, keepdims=True)


def load_receptor_estimator(
    set_name='govardoskii', 
    bg_name='morning', 
    bg_int=1000, 
    exclude_opsins=None, 
    capitalize=False,
    **kwargs
):
    from chreyesees.colormedulla import SensitivitySet
    
    S, S_var = SensitivitySet().fetch1_sense(set_name, return_var=True)
    I = load_led_spectra()
    
    if exclude_opsins:
        S = S.drop(columns=exclude_opsins)
        S_var = S_var.drop(columns=exclude_opsins)
        
    est = ReceptorEstimator(
        S.to_numpy().T, 
        labels=(S.columns.to_numpy() if not capitalize else [col.capitalize() for col in S.columns]), 
        domain=WLS, 
        sources=I.T, 
        sources_labels=LEDS, 
        filters_uncertainty=np.sqrt(S_var.to_numpy().T),
        **kwargs
    )
    if bg_name is not None:
        bg_ints = load_bg_ints(bg_name, bg_int)
        est.register_system_adaptation(bg_ints)
        
    return est


def load_bg_spectra(bg_name, bg_int):
    bg_ints = load_bg_ints(bg_name, bg_int)
    led_spectra = load_led_spectra()
    return bg_ints @ led_spectra.T


def load_bg_ints(bg_name, bg_int):
    from chreyesees.colormedulla import Background
    bg_ints = (Background & {'bg_name': bg_name, 'bg_int': bg_int}).fetch1(*BG_LEDS)
    bg_ints = np.array(bg_ints)
    return bg_ints


def load_spectrum(sigma=0, bg_name='morning'):
    if isinstance(bg_name, Number):
        return planck_blackbody(WLS, bg_name)
    
    Sp = load_spitschan2016(True, label_cols=['solar_elevation'])
    sp = interp1d(Sp.index.to_numpy(), Sp.T.to_numpy())(WLS)
    
    if bg_name is None:
        # mean spectrum
        sp = sp.mean(0)
    elif bg_name == 'morning':
        # morning according to solar elevation
        sp = sp[(Sp.columns > 0) & (Sp.columns < 12) & ~np.any(sp == 0, axis=-1)].mean(0)
    elif bg_name == 'dawn-earlier':
        # dawn according to solar elevation
        sp = sp[(Sp.columns > -12) & (Sp.columns < 0) & ~np.any(sp == 0, axis=-1)].mean(0)
    else:
        raise ValueError(f"unknown bg_name {bg_name}")
    
    if not sigma:
        return sp
    return gaussian_filter1d(sp, sigma=sigma/np.mean(np.diff(WLS)), axis=-1)

def load_bg_qs(bg_name, bg_int, set_name, return_var=False):
    from chreyesees.colormedulla import BackgroundCaptures
    bg_entry = BackgroundCaptures & {
        'bg_name': bg_name, 
        'bg_int': bg_int, 
        'set_name': set_name
    }
    bg_qs = np.array(bg_entry.fetch1(*BG_QS))
    if return_var:
        bg_qs_var = np.array(bg_entry.fetch1(*BG_QS_VAR))
        return bg_qs, bg_qs_var
    return bg_qs


def hist_angles(x, centers, binsize, stat=np.sum):
    hist = {}
    for center in centers:
        e1 = center - binsize / 2
        e2 = center + binsize / 2
        
        if (e1 >= 0) and (e2 <= (2*np.pi)):
            hist[(e1, e2, center)] = stat((x >= e1) & (x < e2))
        else:
            e1 = e1 % (2*np.pi)
            e2 = e2 % (2*np.pi)
            hist[(e1, e2, center)] = stat(
                (x >= e1) | (x < e2)
            )

    hist = pd.Series(hist)
    hist.index.names = ('e1', 'e2', 'emean')
    return hist


def bootstrapped_stat(
    x, n=10000, stat=np.nanmean, 
    along_axis=False,
    ci=95, seed=None
):
    """Calculate bootstrapped statistic

    Parameters
    ----------
    x : array-like of shape (..., n_samples)
        Array of values to calculate statistic on.
    n : int, optional
        Number of bootstrap iterations, by default 10000
    stat : callable, optional
        function used to calulcate statistic. Must
        accept an axis argument. Defaults to np.nanmean.
    ci : int, optional
        Confiden interval, by default 95
    seed : int, optional
        Seed to use to draw bootstrapped samples, by default None

    Returns
    -------
    percentiles : numpy.ndarray of shape (2, ...)
    """
    x = np.asarray(x)
    rng = default_rng(seed)
    idcs = rng.integers(0, x.shape[-1], size=(n, x.shape[-1]))
    if isinstance(stat, str):
        stat = getattr(np, stat)
    if along_axis:
        bs_stats = np.apply_along_axis(stat, -1, x[..., idcs])
    else:
        bs_stats = stat(x[..., idcs], axis=-1)

    return np.nanpercentile(
        bs_stats, q=((100-ci)/2, ci+(100-ci)/2), 
        axis=-1
    )
    
    
def calculate_capture(X, set_name='standard', return_var=False, **kwargs):
    """Calculate absolute capture.

    Parameters
    ----------
    X : numpy.ndarray of shape (..., n_leds)
        Led intensities
    set_name : str, optional
        Name of sensitivity set in database, by default 'standard'
    """
    X = np.asarray(X)
    est = load_receptor_estimator(set_name, **kwargs)
    spectra = X @ est.sources
    if return_var:
        return est.capture(spectra), est.uncertainty_capture(spectra)
    return est.capture(spectra)