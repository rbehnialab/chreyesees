
import pathlib
import datajoint as dj
import os
import json
import numpy as np
import proplot as pplt
from .datajoint_attributes import pickletype

pplt.rc.update({
    'tick.minor': False, 
    'grid': False, 
    'label.size': 'small', 
    'axes.alpha': 0.0,
    'tick.labelsize': 'x-small', 
    'meta.linewidth': 1, 
    # 'geo.extent': 'auto', 
    'tick.labelpad': 0.5, 
    'legend.fontsize': 'x-small', 
    'axes.unicode_minus': False
})

os.environ["DJ_SUPPORT_ADAPTED_TYPES"] = "TRUE"

def read_json(filepath):
	with open(filepath, 'r') as f:
		data = json.load(f)
	return data

MASTER_FOLDER = os.path.dirname(os.path.dirname(__file__))

CONFIG_JSON = os.path.join(MASTER_FOLDER, 'config.json')

assert os.path.exists(CONFIG_JSON), "No `config.json` file found. Please follow the instruction in the README.md file."

DJ_CONFIG = read_json(CONFIG_JSON)
DATA_FOLDERPATH = DJ_CONFIG.pop('folderpath')
PLOT_FOLDERPATH = pathlib.Path(DJ_CONFIG.pop('plotpath'))

assert DATA_FOLDERPATH is not None, "`folderpath` not provided in json. Please follow the instruction in the README.md file."
assert PLOT_FOLDERPATH is not None, "`plotpath` not provided in json. Please follow the instruction in the README.md file."

dj.config.update(DJ_CONFIG)
dj.config['backup_context'] = {
    'pickletype': pickletype
}

# PLOT subfolders
DESCRIPTPATH = PLOT_FOLDERPATH / 'descriptplots'
ENCODINGPATH = PLOT_FOLDERPATH / 'encodingplots'
MODELPATH = PLOT_FOLDERPATH / 'model_results'
DESCRIPTPATH.mkdir(parents=True, exist_ok=True)
ENCODINGPATH.mkdir(parents=True, exist_ok=True)
MODELPATH.mkdir(parents=True, exist_ok=True)

# OTHER GLOBAL VARIABLES
WLS = np.arange(300, 700)
# LED names
LEDS = ['duv', 'uv', 'violet', 'rblue', 'lime', 'orange']
LEDS_CMAP = {
    "duv": "magenta",
    "uv": "purple",
    "violet": "blueviolet",
    "rblue": "navy",
    "lime": "springgreen",
    "orange": "orange"
}
# Single letter led names
LETTER_LEDS = [led[:1].upper() if (led != 'rblue') else 'B' for led in LEDS]
LETTER_CMAP = dict(zip(LETTER_LEDS, LEDS_CMAP.values()))
# background led names
BG_LEDS = [f'bg_{led}' for led in LEDS]
# opsin names
OPSINS = ['rh1', 'rh3', 'rh4', 'rh5', 'rh6']
# capture names
QS = [f'q_{q}' for q in OPSINS]
QS_VAR = [f"{rh}_var" for rh in QS]
# background capture names
BG_QS = [f'bg_{q}' for q in OPSINS]
BG_QS_VAR = [f"{rh}_var" for rh in BG_QS]
# interpolated values for PSTHs in ms
TINTERP = np.arange(-500, 2000, 25)
TINTERP_S = TINTERP / 1000
# amplitude calculation
SMOOTHING_WINDOW = 0.5
AMP_INTERVAL = (350, 500)
OFF_INTERVAL = (700, 850)
STIM_INTERVAL = (0, 500)
BASELINE_INTERVAL = (-350, -100)
# calculating null amplitude distribution
NULL_T_BEFORE = 5000
NULL_BEFORE_OFFSET = 1000
NULL_S_BEFORE = 100
NULL_T_AFTER = 5000
NULL_S_AFTER = 100
CI = 95
ROC_FALSE_POS = np.linspace(0, 1, 21)
ROC_TRUE_POS = np.linspace(0, 1, 21)
# 
SNR_THRESH = 2
# inferred
TBOOL_AMP = (TINTERP >= AMP_INTERVAL[0]) & (TINTERP <= AMP_INTERVAL[1])
TBOOL_BASELINE = (TINTERP >= BASELINE_INTERVAL[0]) & (TINTERP <= BASELINE_INTERVAL[1])
TBOOL_OFF =  (TINTERP >= OFF_INTERVAL[0]) & (TINTERP <= OFF_INTERVAL[1])