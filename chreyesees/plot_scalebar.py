import seaborn as sns

# from axolotl import utils
from dreye.api.utils import signif


def scalebar(
    ax, xsize=None, ysize=None, xunits=None, yunits=None,
    xleft=-0.01, ybottom=-0.01, bottom=False, right=True,
    left=False, top=True, ylim=None, xlim=None,
    infer_sizes=False, offset=None,
    yformat=False, xformat=False,
    **kwargs
):
    """
    """
    kwargs['fontsize'] = kwargs.get('fontsize', 'x-small')
    if ylim is None:
        ylim = ax.get_ylim()
    else:
        ax.set_ylim(*ylim)

    if xlim is None:
        xlim = ax.get_xlim()
    else:
        ax.set_xlim(*xlim)

    # infer xsize and ysize from ticks
    if infer_sizes:
        if xsize is None and infer_sizes != 'infer_y':
            xticks = ax.get_xticks()
            xsize = xticks[1] - xticks[0]
            xsize = signif(xsize, p=2)
        if ysize is None and infer_sizes != 'infer_x':
            yticks = ax.get_yticks()
            ysize = yticks[1] - yticks[0]
            ysize = signif(ysize, p=2)

    xleft, ybottom = axis_data_coords_sys_transform(ax, xleft, ybottom)

    if xsize is not None:

        ax.set_xticks([xlim[0], xlim[0]+xsize])
        half = (xlim[0]*2+xsize)/2
        if xunits is not None:
            if xformat:
                ax.text(
                    half, ybottom,
                    xunits.format(xsize),
                    verticalalignment='top',
                    horizontalalignment='center',
                    **kwargs
                )
            else:
                ax.text(
                    half, ybottom,
                    '{} {}'.format(xsize, xunits),
                    verticalalignment='top',
                    horizontalalignment='center',
                    **kwargs
                )
        else:
            ax.text(
                half, ybottom,
                '{}'.format(xsize),
                verticalalignment='top',
                horizontalalignment='center',
                **kwargs
            )

    if ysize is not None:

        ax.set_yticks([ylim[0], ylim[0]+ysize])
        half = (ylim[0]*2+ysize)/2
        if yunits is not None:
            if yformat:
                ax.text(
                    xleft, half,
                    yunits.format(ysize),
                    verticalalignment='center',
                    horizontalalignment='right',
                    rotation=90,
                    **kwargs
                )
            else:
                ax.text(
                    xleft, half,
                    '{} {}'.format(ysize, yunits),
                    verticalalignment='center',
                    horizontalalignment='right',
                    rotation=90,
                    **kwargs
                )
        else:
            ax.text(
                xleft, half,
                '{}'.format(ysize),
                verticalalignment='center',
                horizontalalignment='right',
                rotation=90,
                **kwargs
            )
    sns.despine(
            ax=ax, trim=True, offset=offset,
            bottom=bottom, top=top, left=left, right=right)

    if xsize is not None:
        ax.set_xticks([])
    if ysize is not None:
        ax.set_yticks([])
    return ax


def axis_data_coords_sys_transform(ax, xin, yin, inverse=False):
    """ inverse = False : Axis => Data
                = True  : Data => Axis
    """
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()

    xdelta = xlim[1] - xlim[0]
    ydelta = ylim[1] - ylim[0]
    if not inverse:
        xout = xlim[0] + xin * xdelta
        yout = ylim[0] + yin * ydelta
    else:
        xdelta2 = xin - xlim[0]
        ydelta2 = yin - ylim[0]
        xout = xdelta2 / xdelta
        yout = ydelta2 / ydelta
    return xout, yout


def panel_letter(
    ax, letter, xpos=-0.1, ypos=1.1,
    weight='bold', size=20, transform='axes', **kwargs
):
    if transform == 'axes':
        transform = ax.transAxes
    ax.text(
        xpos, ypos, letter, size=size, weight=weight,
        transform=transform, **kwargs
    )
    return ax
