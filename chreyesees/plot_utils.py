import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def add_identity(axes=None, *line_args, **line_kwargs):
    if axes is None:
        axes = plt.gca()
    identity, = axes.plot([], [], *line_args, **line_kwargs)
    def callback(axes):
        low_x, high_x = axes.get_xlim()
        low_y, high_y = axes.get_ylim()
        low = max(low_x, low_y)
        high = min(high_x, high_y)
        identity.set_data([low, high], [low, high])
    callback(axes)
    axes.callbacks.connect('xlim_changed', callback)
    axes.callbacks.connect('ylim_changed', callback)
    return axes


def add_inverse_identity(axes=None, *line_args, **line_kwargs):
    if axes is None:
        axes = plt.gca()
    identity, = axes.plot([], [], *line_args, **line_kwargs)
    def callback(axes):
        low_x, high_x = axes.get_xlim()
        low_y, high_y = axes.get_ylim()
        low = min(high_x, -low_y)
        high = max(low_x, -high_y)
        print(low, high)
        identity.set_data([low, high], [-low, -high])
    callback(axes)
    axes.callbacks.connect('xlim_changed', callback)
    axes.callbacks.connect('ylim_changed', callback)
    return axes


def simplex_plot_responses(data, est, y='r', q=99, ax=None, simplex_kws={}, **kwargs):
    # seaborn compatible plotting
    if ax is None:
        ax = plt.gca()
    labels = est.labels
    try:
        Q = data[labels].to_numpy()
    except:
        Q = data[[label.lower() for label in labels]]
    y = data[y].to_numpy()
    ymax = np.percentile(np.abs(y), q=q)
    y = y / ymax
    est.simplex_plot(
        Q, add_hull=False, add_center=False,
        cmap_B='coolwarm', 
        c=y, vmin=-1, vmax=1, ax=ax, 
        **simplex_kws
    )
    return ax


def percentile_plot(y, q, ax=None, **kwargs):
    if ax is None:
        ax = plt.gca()
    ax.axhline(np.percentile(y, q=q), **kwargs)
    return ax


def fill_between_plot(
    x, y, low, high, ax=None, 
    exclusive_line={},
    exclusive_betw={}, 
    **kwargs):
    if ax is None:
        ax = plt.gca()
        
    ax.fill_between(x, low, high, alpha=0.5, **{**kwargs, **exclusive_betw})
    ax.plot(x, y, **{**kwargs, **exclusive_line})
    return ax


def equal_all(ax=None, aspect=True, sep=False):
    if ax is None:
        ax = plt.gca()
        
    lims = np.array([ax.get_ylim(), ax.get_xlim()])
    
    if sep:
        maxi = lims[:, 1].max()
        mini = lims[:, 0].min()
        ax.set_ylim(mini, maxi)
        ax.set_xlim(mini, maxi)
    else:
        maxi = np.abs(lims).max()
        
        ax.set_ylim(-maxi, maxi)
        ax.set_xlim(-maxi, maxi)
    
    if aspect:
        ax.set_aspect('equal')
    
    return maxi


def vertical_histogram(
    data, x, y, 
    arange=None,
    bins=10, 
    ax=None, 
    hist_kwargs={}, 
    normalize=True, 
    mean_kwargs={}, 
    mean_dist_kwargs={},
    mean=None,
    mean_separate=False,
    order=None,
    hue=None,
    palette_dict=None,
    add_vertical=False,
    vertical_kwargs={},
    mean_same_color=True,
    estimator='mean',
    **kwargs
):
    if ax is None:
        ax = plt.gca()

    # Computed quantities to aid plotting
    if arange is None:
        arange = (data[y].min(), data[y].max())
        
    if order is None:
        generator = data.groupby(x)
    else:
        generator = [(ix, data[(data[x] == ix)].copy()) for ix in order]
        
    if (hue is not None):
        assert palette_dict is not None, "Supply palette dictionary"
        generator = [
            ((name, hue_name), hue_d)
            for name, d in generator
            for hue_name, hue_d in d.groupby(hue)
        ]
            
    binned_data_sets = {
        name : np.histogram(d[y], range=arange, bins=bins, **hist_kwargs)[0]
        for name, d in generator
    }
    estimator = getattr(np, estimator)
    if mean is None:
        means = {
            name: estimator(d[y])
            for name, d in generator
        }
    elif mean_separate:
        means_dist = {
            name: estimator(d[y])
            for name, d in generator
        }
        means = {
            name: estimator(d[mean])
            for name, d in generator
        }
    else:
        means = {
            name: estimator(d[mean])
            for name, d in generator
        }
    if normalize:
        binned_data_sets = {
            k: v / np.maximum(np.sum(v), 1e-10)
            for k, v in binned_data_sets.items()
        }
    # the maximum histogram height value in each group
    binned_maximums = np.max(list(binned_data_sets.values()), axis=1)
    # get the locations of each 
    lemax = np.nanmax(binned_maximums)
    x_locations = np.arange(len(binned_data_sets)) * lemax * 1.2

    # The bin_edges are the same for all of the histograms
    bin_edges = np.linspace(*arange, bins + 1)
    centers = np.mean([bin_edges[:-1], bin_edges[1:]], axis=0)
    heights = np.diff(bin_edges)

    # Cycle through and plot each histogram
    for x_loc, label in zip(x_locations, binned_data_sets):
        if hue is not None:
            kwargs['color'] = palette_dict[label[1]]
            if mean_same_color:
                mean_kwargs['color'] = palette_dict[label[1]]
            if mean_separate and mean is not None:
                mean_dist_kwargs['color'] = mean_dist_kwargs.get('color', palette_dict[label[1]])
                mean_dist_kwargs = {**mean_kwargs, **mean_dist_kwargs}
        binned_data = binned_data_sets[label]
        lefts = x_loc - 0.5 * binned_data
        ax.barh(
            centers, binned_data,
            height=heights, left=lefts, 
            **kwargs
        )
        if mean_separate and mean is not None:
            if mean_same_color:
                mean_dist_kwargs['linestyle'] = '--'
            ax.plot(
                [x_loc-0.5*lemax, x_loc+0.5*lemax], 
                [means_dist[label], means_dist[label]], 
                **mean_dist_kwargs
            )
        ax.plot(
            [x_loc-0.5*lemax, x_loc+0.5*lemax], 
            [means[label], means[label]], 
            **mean_kwargs
        )
        if add_vertical:
            ax.axvline(x_loc, **vertical_kwargs)
        
    if hue is None:
        ax.set_xticks(x_locations)
        ax.set_xticklabels(list(binned_data_sets))
    else:
        new_x_locs = pd.DataFrame(
            np.stack([x_locations, [label[0] for label in binned_data_sets]], axis=-1),
            columns=['xloc', 'label']
        ).groupby(
            'label'
        )['xloc'].mean()
        ax.set_xticks(new_x_locs.to_numpy())
        ax.set_xticklabels(new_x_locs.index)

    ax.set_xlim(np.min(x_locations)-1.4*lemax, np.max(x_locations)+1.4*lemax)
    return ax
        