#!/usr/bin/env python

import setuptools
import os

path = os.path.abspath(os.path.dirname(__file__))

# User README.md as long description
with open("README.md", encoding="utf-8") as f:
    README = f.read()


setuptools.setup(
    name='chreyesees',
    version='0.0.0',
    description='ChreyeSees',
    long_description=README,
    author='Matthias Christenson',
    author_email='gucky@gucky.eu',
    packages=setuptools.find_packages(exclude=['tests', 'docs', 'data']),
    include_package_data=True,
)
